﻿#include "AppMainWindow.h"
#include "ui_AppMainWindow.h"
// Qt 相关
#include <QMessageBox>
//
#include "SARibbonBar.h"
//插件相关
#include "DAAppPluginManager.h"
#include "DAPluginManager.h"
#include "DAAbstractPlugin.h"
#include "DAAbstractNodePlugin.h"
//界面相关
#include "DAAppCore.h"
#include "DAAppUI.h"
#include "DAAppDockingArea.h"
#include "DAAppRibbonArea.h"

//对话框
#include "DAPluginManagerDialog.h"

//节点相关
#include "DANodeMetaData.h"
#include "DAAbstractNodeFactory.h"
#include "DAAbstractNodeWidget.h"

//
#include "DAGraphicsItemFactory.h"
#include "DAWorkFlowNodeListWidget.h"
#include "DAWorkFlowOperateWidget.h"
#include "DAChartOperateWidget.h"
#include "DAChartManageWidget.h"
#include "DADataManageWidget.h"
#include "DADataOperateWidget.h"
#include "DAWorkFlowOperateWidget.h"
#include "DAWorkFlowOperateWidget.h"
#include "DAMessageLogViewWidget.h"
//===================================================
// using DA namespace -- 禁止在头文件using！！
//===================================================

using namespace DA;

//===================================================
// AppMainWindow
//===================================================
AppMainWindow::AppMainWindow(QWidget* parent) : SARibbonMainWindow(parent)

{
    //建立ribbonArea，此函数的构造函数会生成界面
    DAAppCore& core = DAAppCore::getInstance();
    core.createUi(this);
    m_ui       = qobject_cast< DAAppUI* >(core.getUiInterface());
    m_dockArea = qobject_cast< DAAppDockingArea* >(m_ui->getDockingArea());
    //首次调用此函数会加载插件，可放置在main函数中调用
    init();
    DAGraphicsItemFactory::initialization();
    retranslateUi();
    ribbonBar()->setRibbonStyle(SARibbonBar::WpsLiteStyleTwoRow);
    showMaximized();
}

AppMainWindow::~AppMainWindow()
{
    //    delete ui;
}

void AppMainWindow::retranslateUi()
{
    m_ui->retranslateUi();
}

void AppMainWindow::changeEvent(QEvent* e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        retranslateUi();
        break;

    default:
        break;
    }
}

void AppMainWindow::setupNodeListWidget()
{
    DAAppPluginManager& plugin               = DAAppPluginManager::instance();
    QList< DAAbstractNodeFactory* > factorys = plugin.getNodeFactorys();
}

void AppMainWindow::init()
{
    DAAppPluginManager& pluginmgr = DAAppPluginManager::instance();
    DAAppCore& core               = DAAppCore::getInstance();
    pluginmgr.initLoadPlugins(&core);
    initUI();
    initConnect();
}

void AppMainWindow::initUI()
{
    //设置dock

    DAAppPluginManager& pluginmgr = DAAppPluginManager::instance();
    //提取所有的元数据
    QList< DANodeMetaData > nodeMetaDatas = pluginmgr.getAllNodeMetaDatas();
    QMap< QString, QList< DANodeMetaData > > groupedNodeMetaDatas;

    for (const DANodeMetaData& md : qAsConst(nodeMetaDatas)) {
        groupedNodeMetaDatas[ md.getGroup() ].append(md);
    }
    //把数据写入toolbox
    m_dockArea->getWorkflowNodeListWidget()->addItems(groupedNodeMetaDatas);
    //此时才创建第一个workflow，这个workflow创建时，插件已经加载好
    m_dockArea->getWorkFlowOperateWidget()->appendWorkflow(tr("untitle"));
}

void AppMainWindow::initConnect()
{
}

void AppMainWindow::onWorkflowFinished(bool success)
{
    if (success) {
        QMessageBox::information(this, tr("infomation"), tr("Topology execution completed"));  //拓扑执行完成
    } else {
        QMessageBox::critical(this, tr("infomation"), tr("Topology execution failed"));  //拓扑执行失败
    }
}
