<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AppMainWindow</name>
    <message>
        <location filename="AppMainWindow.ui" line="14"/>
        <source>MethodMainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AppMainWindow.ui" line="24"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="AppMainWindow.ui" line="33"/>
        <source>save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="AppMainWindow.ui" line="42"/>
        <source>saveAs</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location filename="AppMainWindow.ui" line="51"/>
        <source>redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AppMainWindow.ui" line="60"/>
        <source>undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AppMainWindow.ui" line="69"/>
        <source>plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AppMainWindow.ui" line="78"/>
        <source>Show Work Flow Area</source>
        <translation>显示流程图区域</translation>
    </message>
    <message>
        <location filename="AppMainWindow.ui" line="87"/>
        <source>Show Chart Area</source>
        <translation>显示图表区域</translation>
    </message>
    <message>
        <location filename="AppMainWindow.ui" line="96"/>
        <source>Show Table Area</source>
        <translation>显示表格区域</translation>
    </message>
    <message>
        <location filename="AppMainWindow.ui" line="105"/>
        <source>Show Infomation Window</source>
        <translation>显示信息窗口</translation>
    </message>
</context>
<context>
    <name>DA::AppMainWindow</name>
    <message>
        <location filename="AppMainWindow.cpp" line="142"/>
        <location filename="AppMainWindow.cpp" line="144"/>
        <source>infomation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AppMainWindow.cpp" line="142"/>
        <source>Topology execution completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AppMainWindow.cpp" line="144"/>
        <source>Topology execution failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DAAppActions</name>
    <message>
        <location filename="DAAppActions.cpp" line="71"/>
        <source>打开</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="72"/>
        <source>保存</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="73"/>
        <source>另存为</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="74"/>
        <source>Rename Columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="75"/>
        <source>设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="77"/>
        <source>添加数据</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="78"/>
        <source>移除数据</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="79"/>
        <source>添加文件夹</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="81"/>
        <source>Remove Row</source>
        <translation>删除行</translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="82"/>
        <source>Remove Column</source>
        <translation>删除列</translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="83"/>
        <source>Remove Cell</source>
        <translation>移除单元格</translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="84"/>
        <source>Insert Row</source>
        <translation>插入行</translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="85"/>
        <source>Insert Row(Above)</source>
        <translation>插入行(上)</translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="86"/>
        <source>Insert Column</source>
        <translation>插入列(右)</translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="87"/>
        <source>Insert Column(Left)</source>
        <translation>插入列(左)</translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="88"/>
        <source>to num</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="89"/>
        <source>cast to num type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="90"/>
        <source>to str</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="91"/>
        <source>cast to string type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="92"/>
        <source>to datetime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="93"/>
        <source>cast to datetime type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="94"/>
        <source>Data Describe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="95"/>
        <source>To Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="97"/>
        <source>绘制矩形</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="98"/>
        <source>绘制文本</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="100"/>
        <source>显示流程图区域</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="101"/>
        <source>显示图标区域</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="102"/>
        <source>显示表格区域</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="103"/>
        <source>显示信息窗口</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="104"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="105"/>
        <source>Show Info Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="106"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="107"/>
        <source>Show Warning Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="108"/>
        <source>Critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="109"/>
        <source>Show Critical Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="110"/>
        <source>显示设置窗口</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="112"/>
        <source>管理插件</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="115"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppActions.cpp" line="118"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DAAppCore</name>
    <message>
        <location filename="DAAppCore.cpp" line="95"/>
        <source>Python interpreter path is %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppCore.cpp" line="102"/>
        <source>Python scripts path is %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppCore.cpp" line="105"/>
        <source>Scripts initialize error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppCore.cpp" line="110"/>
        <source>Initialize python environment error:%1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DAAppDataManager</name>
    <message>
        <location filename="DAAppDataManager.cpp" line="64"/>
        <source>begin import file:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppDataManager.cpp" line="67"/>
        <source>file:%1,conver to dataframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppDataManager.cpp" line="76"/>
        <source>can not import file:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppDataManager.cpp" line="104"/>
        <source>add datas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppDataManager.cpp" line="129"/>
        <source>remove datas</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DAAppDataManagerTree</name>
    <message>
        <location filename="DAAppDataManagerTree.cpp" line="192"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppDataManagerTree.cpp" line="193"/>
        <source>property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppDataManagerTree.cpp" line="495"/>
        <location filename="DAAppDataManagerTree.cpp" line="507"/>
        <source>The data(%1) cannot find its corresponding item in the data management tree during the removal process</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DAAppDockingArea</name>
    <message>
        <location filename="DAAppDockingArea.cpp" line="48"/>
        <source>workflow node</source>
        <translation>工具箱</translation>
    </message>
    <message>
        <location filename="DAAppDockingArea.cpp" line="49"/>
        <source>charts manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppDockingArea.cpp" line="50"/>
        <source>datas manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppDockingArea.cpp" line="51"/>
        <source>workflow operate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppDockingArea.cpp" line="52"/>
        <source>chart operate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppDockingArea.cpp" line="53"/>
        <source>data operate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppDockingArea.cpp" line="54"/>
        <source>setting</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="DAAppDockingArea.cpp" line="55"/>
        <source>log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppDockingArea.cpp" line="306"/>
        <source>property</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DAAppPluginManager</name>
    <message>
        <location filename="DAAppPluginManager.cpp" line="88"/>
        <source>succeed load plugin %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DAAppRibbonArea</name>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="105"/>
        <source>文件</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="107"/>
        <source>主页</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="108"/>
        <source>File Operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="109"/>
        <location filename="DAAppRibbonArea.cpp" line="112"/>
        <source>Data Operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="110"/>
        <source>Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="111"/>
        <source>数据</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="113"/>
        <source>视图</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="114"/>
        <source>编辑</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="115"/>
        <source>单位制</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="116"/>
        <source>帮助</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="117"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="118"/>
        <location filename="DAAppRibbonArea.cpp" line="309"/>
        <source>DataFrame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="119"/>
        <location filename="DAAppRibbonArea.cpp" line="310"/>
        <source>Operate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="120"/>
        <location filename="DAAppRibbonArea.cpp" line="312"/>
        <source>Axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="121"/>
        <location filename="DAAppRibbonArea.cpp" line="122"/>
        <location filename="DAAppRibbonArea.cpp" line="322"/>
        <location filename="DAAppRibbonArea.cpp" line="326"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="123"/>
        <location filename="DAAppRibbonArea.cpp" line="336"/>
        <source>Statistic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="125"/>
        <source>Data Work Flow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="565"/>
        <source>project file(*.%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="578"/>
        <source>Question</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="579"/>
        <source>Another project already exists. Do you want to replace it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="590"/>
        <source>failed to load project file:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="603"/>
        <source>saving project,path is %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="606"/>
        <source>Save Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="608"/>
        <source>project file (*.%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="617"/>
        <source>Project saved failed!,path is %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="620"/>
        <source>Project saved successfully,path is %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="768"/>
        <source>%1_Describe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAAppRibbonArea.cpp" line="769"/>
        <source>Generate descriptive statistics that summarize the central tendency, dispersion and shape of a [%1]’s distribution, excluding NaN values</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DADataManageTableView</name>
    <message>
        <location filename="DADataManageTableView.cpp" line="56"/>
        <source>The item is selected in the data management table, but the corresponding data cannot be obtained</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DADataManageTreeView</name>
    <message>
        <location filename="DADataManageTreeView.cpp" line="62"/>
        <source>untitle floder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DADataManageWidget</name>
    <message>
        <location filename="DADataManageWidget.cpp" line="126"/>
        <source>Please select the data item to remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DADataManageWidget.cpp" line="134"/>
        <source>show datas in table view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DADataManageWidget.cpp" line="135"/>
        <source>show datas in tree view</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DADataManagerTableModel</name>
    <message>
        <location filename="Models/DADataManagerTableModel.cpp" line="32"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Models/DADataManagerTableModel.cpp" line="34"/>
        <source>type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DADataOperateOfDataFrameWidget</name>
    <message>
        <location filename="DADataOperateOfDataFrameWidget.cpp" line="163"/>
        <source>warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DADataOperateOfDataFrameWidget.cpp" line="164"/>
        <source>The name of the new column to be inserted must be specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DADataOperateOfDataFrameWidget.cpp" line="193"/>
        <source>please select valid data cells</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DADataOperateOfDataFrameWidget.cpp" line="217"/>
        <location filename="DADataOperateOfDataFrameWidget.cpp" line="307"/>
        <location filename="DADataOperateOfDataFrameWidget.cpp" line="335"/>
        <location filename="DADataOperateOfDataFrameWidget.cpp" line="370"/>
        <location filename="DADataOperateOfDataFrameWidget.cpp" line="406"/>
        <source>please select valid column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DADataOperateOfDataFrameWidget.cpp" line="241"/>
        <source>please select valid cell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DADataOperateOfDataFrameWidget.cpp" line="275"/>
        <source>table have not column</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DADataOperateWidget</name>
    <message>
        <location filename="DADataOperateWidget.cpp" line="77"/>
        <source>removing a widget that does not exist in tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DADataOperateWidget.cpp" line="112"/>
        <source>[deleted]</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DAMessageLogsModel</name>
    <message>
        <location filename="Models/DAMessageLogsModel.cpp" line="66"/>
        <source>date time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Models/DAMessageLogsModel.cpp" line="68"/>
        <location filename="Models/DAMessageLogsModel.cpp" line="73"/>
        <source>message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DANodeTreeWidget</name>
    <message>
        <location filename="DANodeTreeWidget.cpp" line="118"/>
        <source>定制系统</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DANodeTreeWidget.cpp" line="124"/>
        <source>压气机</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DANodeTreeWidget.cpp" line="128"/>
        <location filename="DANodeTreeWidget.cpp" line="146"/>
        <source>叶片</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DANodeTreeWidget.cpp" line="131"/>
        <location filename="DANodeTreeWidget.cpp" line="149"/>
        <source>机匣</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DANodeTreeWidget.cpp" line="134"/>
        <location filename="DANodeTreeWidget.cpp" line="152"/>
        <source>轮盘</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DANodeTreeWidget.cpp" line="138"/>
        <source>燃烧室</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DANodeTreeWidget.cpp" line="142"/>
        <source>涡轮</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DANodeTreeWidget.cpp" line="156"/>
        <source>传动系统</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DANodeTreeWidget.cpp" line="160"/>
        <source>轴承</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DANodeTreeWidget.cpp" line="163"/>
        <source>齿轮</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DANodeTreeWidget.cpp" line="166"/>
        <source>转子连接件</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DANodeTreeWidget.cpp" line="170"/>
        <source>已有方案</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DANodeTreeWidget.cpp" line="265"/>
        <source>can not load setting file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DANodeTreeWidget.cpp" line="271"/>
        <source>can not load setting file,error info is:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DANodeTreeWidget.cpp" line="277"/>
        <source>setting file,loss fix-project tag</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DAPluginManagerDialog</name>
    <message>
        <location filename="DAPluginManagerDialog.cpp" line="44"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginManagerDialog.cpp" line="44"/>
        <source>version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginManagerDialog.cpp" line="44"/>
        <source>is loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginManagerDialog.cpp" line="44"/>
        <source>description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginManagerDialog.cpp" line="50"/>
        <source>node plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginManagerDialog.cpp" line="53"/>
        <source>plugin name is %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginManagerDialog.cpp" line="57"/>
        <source>is load</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DAProject</name>
    <message>
        <location filename="DAProject.cpp" line="100"/>
        <source>save workflow extern info cost: %1 ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="103"/>
        <source>save workflow nodes cost: %1 ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="106"/>
        <source>save workflow links cost: %1 ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="109"/>
        <source>save special item cost: %1 ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="112"/>
        <source>save workflow factory info cost: %1 ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="115"/>
        <source>save secen info cost: %1 ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="131"/>
        <source>load workflow extern info cost: %1 ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="134"/>
        <source>load nodes occurce error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="136"/>
        <source>load workflow nodes cost: %1 ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="139"/>
        <source>load nodes link occurce error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="141"/>
        <source>load workflow links cost: %1 ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="144"/>
        <source>load special item occurce error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="146"/>
        <source>load special item cost: %1 ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="151"/>
        <source>load factorys occurce error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="153"/>
        <source>load workflow factory info cost: %1 ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="156"/>
        <source>load scene info occurce error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="158"/>
        <source>load secen info cost: %1 ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="198"/>
        <source>find unknow tag &lt;%1&gt; under &lt;factorys&gt; element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="204"/>
        <source>can not find factory prototypes = %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="251"/>
        <source>node&apos;s id=%1 can not conver to qulonglong type ,will skip this node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="261"/>
        <source>workflow can not create note by metadata(prototype=%1,name=%2,group=%3),will skip this node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="319"/>
        <source>The attribute %1=%2 under the tag %3 cannot be converted to double </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="382"/>
        <location filename="DAProject.cpp" line="406"/>
        <source>node(prototype=%1,name=%2,group=%3) %4 tag loss child tag &lt;name&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="491"/>
        <source>node metadata(prototype=%1,name=%2,group=%3) can not create graphics item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="579"/>
        <location filename="DAProject.cpp" line="586"/>
        <source>link info can not find node in workflow,id = %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="592"/>
        <source>can not get item by node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="597"/>
        <source>link item can not attach from node item(%1) with key=%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="601"/>
        <source>link item can not attach to node item(%1) with key=%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="853"/>
        <source>open failed,path is %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DARenameColumnsNameDialog</name>
    <message>
        <location filename="Dialog/DARenameColumnsNameDialog.cpp" line="47"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DARenameColumnsNameDialog.cpp" line="91"/>
        <source>warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DARenameColumnsNameDialog.cpp" line="92"/>
        <source>Duplicate column name &quot;%1&quot;,Please reset the column name of column %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DATreeModel</name>
    <message>
        <location filename="Models/DATreeModel.cpp" line="113"/>
        <source>DATreeModel get invalid item</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DAWorkFlowOperateWidget</name>
    <message>
        <location filename="DAWorkFlowOperateWidget.cpp" line="230"/>
        <source>no workflow set</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DAChartManageWidget</name>
    <message>
        <location filename="DAChartManageWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DAChartOperateWidget</name>
    <message>
        <location filename="DAChartOperateWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DADataManageWidget</name>
    <message>
        <location filename="DADataManageWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DADataOperateOfDataFrameWidget</name>
    <message>
        <location filename="DADataOperateOfDataFrameWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DADataOperateWidget</name>
    <message>
        <location filename="DADataOperateWidget.ui" line="20"/>
        <source>Data Operator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DADataOperateWidget.ui" line="23"/>
        <source>This is the data operation window, which is specially responsible for data operation and display</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DADialogDataframeColumnCastToDatetime</name>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If True and no format is given, attempt to infer the format of the datetime strings, and if it can be inferred, switch to a faster method of parsing them. In some cases this can increase the parsing speed by ~5-10x.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="23"/>
        <source>infer datetime format </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="30"/>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="38"/>
        <source>format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="48"/>
        <source>%d/%m/%Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="71"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If True, require an exact format match.&lt;/p&gt;&lt;p&gt;If False, allow the format to match anywhere in the target string.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="74"/>
        <source>exact </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="90"/>
        <source>errors:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="96"/>
        <source>invalid parsing will raise an exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="103"/>
        <source> invalid parsing will be set as NaN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="113"/>
        <source>invalid parsing will return the input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="125"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Define the reference date. The numeric values would be parsed as number of units (defined by unit) since this reference date.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="128"/>
        <source>origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="134"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;origin is set to 1970-01-01&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="137"/>
        <source>unix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="147"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;unit must be ‘D’, and origin is set to beginning of Julian Calendar. Julian day number 0 is assigned to the day starting at noon on January 1, 4713 BC&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="150"/>
        <source>julian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="160"/>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="169"/>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="179"/>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="189"/>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="202"/>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="212"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;unit of the arg (D,s,ms,us,ns) denote the unit, which is an integer or float number. This will be based off the origin. Example, with unit=’ms’ and origin=’unix’ (the default), this would calculate the number of milliseconds to the unix epoch start.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="163"/>
        <source>unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="172"/>
        <source>D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="182"/>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="192"/>
        <source>ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="205"/>
        <source>us</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="215"/>
        <source>ns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="227"/>
        <source>parse set:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="245"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Specify a date parse order&lt;/p&gt;&lt;p&gt;If True, parses dates with the day first, eg 10/11/12 is parsed as 2012-11-10. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; color:#afaf00;&quot;&gt;Warning&lt;/span&gt;:&lt;span style=&quot; font-style:italic;&quot;&gt; dayfirst=True is not strict, but will prefer to parse with day first (this is a known bug, based on dateutil behavior)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="248"/>
        <source>day first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="255"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Specify a date parse order&lt;/p&gt;&lt;p&gt;- If True parses dates with the year first, eg 10/11/12 is parsed as 2010-11-12.&lt;/p&gt;&lt;p&gt;- If both dayfirst and yearfirst are True, yearfirst is preceded (same as dateutil).&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; color:#b6a80b;&quot;&gt;Warning&lt;/span&gt;:&lt;span style=&quot; font-style:italic;&quot;&gt; yearfirst=True is not strict, but will prefer to parse with year first (this is a known bug, based on dateutil behavior).&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="258"/>
        <source>year first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="268"/>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="283"/>
        <source>utc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="280"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Return UTC DatetimeIndex if True (converting any tz-aware datetime.datetime objects as well).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="293"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If True, use a cache of unique, converted dates to apply the datetime conversion. May produce significant speed-up when parsing duplicate date strings, especially ones with timezone offsets.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="296"/>
        <source>cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="334"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToDatetime.ui" line="345"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DADialogDataframeColumnCastToNumeric</name>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="20"/>
        <source>errors:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="43"/>
        <source>invalid parsing will raise an exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="50"/>
        <source> invalid parsing will be set as NaN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="60"/>
        <source>invalid parsing will return the input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="72"/>
        <source>downcast:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="107"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="120"/>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="130"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;smallest signed int dtype (min.: np.int8)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="123"/>
        <source>integer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="133"/>
        <source>signed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="140"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;smallest unsigned int dtype (min.: np.uint8)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="143"/>
        <source>unsigned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="150"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;smallest float dtype (min.: np.float32)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="153"/>
        <source>float</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="193"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogDataframeColumnCastToNumeric.ui" line="204"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DADialogInsertNewColumn</name>
    <message>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="14"/>
        <source>Insert New Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="32"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="59"/>
        <source>dtype</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="84"/>
        <source>Fill Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="90"/>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="110"/>
        <source>Fill in the same value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="100"/>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="129"/>
        <source>Generate growth value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="116"/>
        <source>default value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="142"/>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="166"/>
        <source>start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="152"/>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="186"/>
        <source>stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="179"/>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="199"/>
        <source>yyyy-MM-dd HH:mm:ss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="241"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DADialogInsertNewColumn.ui" line="248"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DAMessageLogViewWidget</name>
    <message>
        <location filename="DAMessageLogViewWidget.ui" line="14"/>
        <source>Log View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAMessageLogViewWidget.ui" line="60"/>
        <location filename="DAMessageLogViewWidget.ui" line="80"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DAPluginManagerDialog</name>
    <message>
        <location filename="DAPluginManagerDialog.ui" line="14"/>
        <source>Plugin Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginManagerDialog.ui" line="57"/>
        <source>ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginManagerDialog.ui" line="67"/>
        <source>cannel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DARenameColumnsNameDialog</name>
    <message>
        <location filename="Dialog/DARenameColumnsNameDialog.ui" line="14"/>
        <source>Rename Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DARenameColumnsNameDialog.ui" line="26"/>
        <source>Table Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DARenameColumnsNameDialog.ui" line="64"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DARenameColumnsNameDialog.ui" line="75"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DASettingContainerWidget</name>
    <message>
        <location filename="DASettingContainerWidget.ui" line="20"/>
        <source>Setting</source>
        <translation>设置</translation>
    </message>
</context>
<context>
    <name>DAWorkFlowNodeItemSettingWidget</name>
    <message>
        <location filename="DAWorkFlowNodeItemSettingWidget.ui" line="14"/>
        <source>Node Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlowNodeItemSettingWidget.ui" line="43"/>
        <source>Node</source>
        <translation>节点</translation>
    </message>
    <message>
        <location filename="DAWorkFlowNodeItemSettingWidget.ui" line="52"/>
        <source>Item</source>
        <translation>节点属性</translation>
    </message>
    <message>
        <location filename="DAWorkFlowNodeItemSettingWidget.ui" line="61"/>
        <source>Link</source>
        <translation>线属性</translation>
    </message>
</context>
<context>
    <name>DAWorkFlowNodeListWidget</name>
    <message>
        <location filename="DAWorkFlowNodeListWidget.ui" line="20"/>
        <source>Node List</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DAWorkFlowOperateWidget</name>
    <message>
        <location filename="DAWorkFlowOperateWidget.ui" line="14"/>
        <source>Work Flow Operate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DAWorkflowTemplateDialog</name>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.ui" line="22"/>
        <source>生成分析流程</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.ui" line="42"/>
        <source>生成报告</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="20"/>
        <source>名称</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="20"/>
        <source>设计准则描述</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="20"/>
        <source>设置准则参数要求</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="20"/>
        <source>仿真结果</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="20"/>
        <source>是否满足</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="22"/>
        <source>静强度储备</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="24"/>
        <source>最大离心径向应力</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="24"/>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="26"/>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="28"/>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="36"/>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="42"/>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="44"/>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="46"/>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="50"/>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="58"/>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="62"/>
        <source>√</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="26"/>
        <source>平均周向应力</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="28"/>
        <source>辐板周向应力</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="30"/>
        <source>变形限制</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="32"/>
        <source>外径残余伸长量</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="32"/>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="38"/>
        <source>×</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="34"/>
        <source>轮盘破裂</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="36"/>
        <source>周向破裂转速储备系数</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="38"/>
        <source>径向破裂转速储备系数</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="40"/>
        <source>有害振动</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="42"/>
        <source>频率裕度</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="44"/>
        <source>高循环变形寿命</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="46"/>
        <source>振动应力</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="48"/>
        <source>低循环</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="50"/>
        <source>平均寿命储备系数</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="52"/>
        <source>损伤容限</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="54"/>
        <source>初始缺陷a下，起转不小于5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="56"/>
        <source>蠕变/应力断裂寿命</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="58"/>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="62"/>
        <source>储备系数</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Dialog/DAWorkflowTemplateDialog.cpp" line="60"/>
        <source>辐板屈曲</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="Commands/DACommandsDataFrame.cpp" line="24"/>
        <source>set dataframe data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Commands/DACommandsDataFrame.cpp" line="55"/>
        <source>insert row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Commands/DACommandsDataFrame.cpp" line="188"/>
        <source>drop dataframe rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Commands/DACommandsDataFrame.cpp" line="226"/>
        <source>drop dataframe columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Commands/DACommandsDataFrame.cpp" line="301"/>
        <source>change column type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Commands/DACommandsDataFrame.cpp" line="387"/>
        <source>cast column to num</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Commands/DACommandsDataFrame.cpp" line="428"/>
        <source>cast column to datetime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Commands/DACommandsDataFrame.cpp" line="469"/>
        <source>set column to index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Commands/DACommandsDataManager.cpp" line="15"/>
        <source>add data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Commands/DACommandsDataManager.cpp" line="35"/>
        <source>add remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="686"/>
        <source>There is a item that is not a DA Graphics Item system and cannot be saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="720"/>
        <location filename="DAProject.cpp" line="731"/>
        <source>Unable to load item information from file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAProject.cpp" line="727"/>
        <source>Cannot create item by class name:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="35"/>
        <source>Kernel initialization failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
