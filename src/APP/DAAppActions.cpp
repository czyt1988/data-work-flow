﻿#include "DAAppActions.h"
#include <QActionGroup>
//===================================================
// using DA namespace -- 禁止在头文件using！！
//===================================================

using namespace DA;

//===================================================
// DAAppActions
//===================================================
DAAppActions::DAAppActions(DAAppUIInterface* u) : DAAppActionsInterface(u)
{
    init();
}

DAAppActions::~DAAppActions()
{
}

void DAAppActions::init()
{
    // Main Category
    actionOpen          = createAction("actionOpen", ":/Icon/Icon/file.svg");
    actionSave          = createAction("actionSave", ":/Icon/Icon/save.svg");
    actionSaveAs        = createAction("actionSaveAs", ":/Icon/Icon/save-as.svg");
    actionAppendProject = createAction("actionAppendProject", ":/Icon/Icon/appendProject.svg");

    actionSaveCurrentWorkflowToFixProject = createAction("actionSaveCurrentWorkflowToFixProject", ":/Icon/Icon/saveCurrentWorkflow.svg");

    actionRedo    = nullptr;
    actionUndo    = nullptr;
    actionSetting = createAction("actionSetting", ":/Icon/Icon/setting.svg");
    // Data Category
    actionAddData       = createAction("actionAddData", ":/Icon/Icon/addData.svg");
    actionRemoveData    = createAction("actionRemoveData", ":/Icon/Icon/removeData.svg");
    actionAddDataFolder = createAction("actionAddDataFolder", ":/Icon/Icon/folder.svg");
    // 数据操作的上下文标签 Data Operate Context Category
    actionRemoveRow          = createAction("actionRemoveRow", ":/Icon/Icon/removeRow.svg");
    actionRemoveColumn       = createAction("actionRemoveColumn", ":/Icon/Icon/removeColumn.svg");
    actionInsertRow          = createAction("actionInsertRow", ":/Icon/Icon/insertRow.svg");
    actionInsertRowAbove     = createAction("actionInsertRowAbove", ":/Icon/Icon/insertRowAbove.svg");
    actionInsertColumnRight  = createAction("actionInsertColumnRight", ":/Icon/Icon/insertColumnRight.svg");
    actionInsertColumnLeft   = createAction("actionInsertColumnLeft", ":/Icon/Icon/insertColumnLeft.svg");
    actionRenameColumns      = createAction("actionRenameColumns", ":/Icon/Icon/renameColumns.svg");
    actionRemoveCell         = createAction("actionRemoveCell", ":/Icon/Icon/removeCell.svg");
    actionCastToNum          = createAction("actionCastToNum", ":/Icon/Icon/castToNum.svg");
    actionCastToString       = createAction("actionCastToString", ":/Icon/Icon/castToString.svg");
    actionCastToDatetime     = createAction("actionCastToDatetime", ":/Icon/Icon/castToDatetime.svg");
    actionCreateDataDescribe = createAction("actionCreateDataDescribe", ":/Icon/Icon/dataDescribe.svg");
    actionChangeToIndex      = createAction("actionChangeToIndex", ":/Icon/Icon/changeToIndex.svg");
    // View Category
    actionShowWorkFlowArea   = createAction("actionShowWorkFlowArea", ":/Icon/Icon/showWorkFlow.svg");
    actionShowChartArea      = createAction("actionShowChartArea", ":/Icon/Icon/showChart.svg");
    actionShowDataArea       = createAction("actionShowDataArea", ":/Icon/Icon/showTable.svg");
    actionShowMessageLogView = createAction("actionShowMessageLogView", ":/Icon/Icon/showInfomation.svg");
    actionShowSettingWidget  = createAction("actionShowSettingWidget", ":/Icon/Icon/showSettingWidget.svg");
    // workflow 编辑
    actionWorkflowNew = createAction("actionNewWorkflow", ":/Icon/Icon/newWorkflow.svg");
    // workflow下面的状态action都是checkable状态的
    actionWorkflowStartEditGroup = new QActionGroup(this);
    actionWorkflowStartDrawRect  = createAction("actionStartDrawRect", ":/Icon/Icon/drawRect.svg", true, false);
    actionWorkflowStartDrawText  = createAction("actionStartDrawText", ":/Icon/Icon/drawText.svg", true, false);
    actionWorkflowStartEditGroup->addAction(actionWorkflowStartDrawRect);
    actionWorkflowStartEditGroup->addAction(actionWorkflowStartDrawText);
    // workflow-背景图相关
    actionWorkflowAddBackgroundPixmap = createAction("actionAddBackgroundPixmap", ":/Icon/Icon/backgroundPixmap.svg");
    actionWorkflowLockBackgroundPixmap = createAction("actionLockBackgroundPixmap", ":/Icon/Icon/lock-bk.svg", true, false);
    actionWorkflowEnableItemMoveWithBackground = createAction("actionEnableItemMoveWithBackground",
                                                              ":/Icon/Icon/itemMoveWithBackground.svg",
                                                              true,
                                                              false);
    // workflow-视图操作
    actionWorkflowShowGrid  = createAction("actionWorkflowShowGrid", ":/Icon/Icon/showGrid.svg", true, true);
    actionWorkflowWholeView = createAction("actionWholeView", ":/Icon/Icon/viewAll.svg");
    actionWorkflowZoomIn    = createAction("actionZoomIn", ":/Icon/Icon/zoomIn.svg");
    actionWorkflowZoomOut   = createAction("actionZoomOut", ":/Icon/Icon/zoomOut.svg");
    actionWorkflowRun       = createAction("actionRunWorkflow", ":/Icon/Icon/run.svg");
    // Config Category
    actionPluginManager = createAction("actionPluginManager", ":/Icon/Icon/plugin.svg");
}

void DAAppActions::retranslateUi()
{
    // Main Category
    actionOpen->setText(tr("Open"));
    actionSave->setText(tr("Save"));
    actionSaveAs->setText(tr("Save As"));
    actionAppendProject->setText(tr("新标签打开工程"));

    actionSaveCurrentWorkflowToFixProject->setText(tr("保存到已有方案"));

    actionRenameColumns->setText(tr("Rename Columns"));
    actionSetting->setText(tr("Setting"));
    // Data Category
    actionAddData->setText(tr("Add Data"));          // cn:添加数据
    actionRemoveData->setText(tr("Remove Data"));    // cn:移除数据
    actionAddDataFolder->setText(tr("Add Folder"));  // cn:新建文件夹
    // 数据操作的上下文标签 Data Operate Context Category
    actionRemoveRow->setText(tr("Remove Row"));                  // cn:删除行
    actionRemoveColumn->setText(tr("Remove Column"));            // cn:删除列
    actionRemoveCell->setText(tr("Remove Cell"));                // cn:移除单元格
    actionInsertRow->setText(tr("Insert Row"));                  // cn:插入行
    actionInsertRowAbove->setText(tr("Insert Row(Above)"));      // cn:插入行(上)
    actionInsertColumnRight->setText(tr("Insert Column"));       // cn:插入列(右)
    actionInsertColumnLeft->setText(tr("Insert Column(Left)"));  // cn:插入列(左)
    actionCastToNum->setText(tr("to num"));
    actionCastToNum->setToolTip(tr("cast to num type"));  // cn:转换为数值类型
    actionCastToString->setText(tr("to str"));
    actionCastToString->setToolTip(tr("cast to string type"));  // cn:转换为字符串类型
    actionCastToDatetime->setText(tr("to datetime"));
    actionCastToDatetime->setToolTip(tr("cast to datetime type"));  // cn:转换为日期类型
    actionCreateDataDescribe->setText(tr("Data Describe"));         // cn:数据描述
    actionChangeToIndex->setText(tr("To Index"));                   // cn:转换为索引
    // workflow 编辑
    actionWorkflowNew->setText(tr("New Workflow"));         // cn:新建工作流
    actionWorkflowStartDrawRect->setText(tr("Draw Rect"));  // cn:绘制矩形
    actionWorkflowStartDrawText->setText(tr("Draw Text"));  // cn:绘制文本
    actionWorkflowShowGrid->setText(tr("Show Grid"));       // cn:显示网格
    actionWorkflowWholeView->setText(tr("Whole View"));     // cn:全部可见
    actionWorkflowZoomOut->setText(tr("Zoom Out"));         // cn:缩小
    actionWorkflowZoomIn->setText(tr("Zoom In"));           // cn:放大
    actionWorkflowRun->setText(tr("Run Workflow"));         // cn:运行工作流

    actionWorkflowAddBackgroundPixmap->setText(tr("Add Background"));                 // cn:添加背景
    actionWorkflowLockBackgroundPixmap->setText(tr("Lock Background"));               // cn:锁定背景
    actionWorkflowEnableItemMoveWithBackground->setText(tr("Move With Background"));  // cn:元件随背景移动
    // View Category
    actionShowWorkFlowArea->setText(tr("Show Work Flow Area"));
    actionShowChartArea->setText(tr("Show Chart Area"));
    actionShowDataArea->setText(tr("Show Table Area"));
    actionShowMessageLogView->setText(tr("Show Infomation Window"));

    actionShowSettingWidget->setText(tr("Show Setting Window"));
    // Config Category
    actionPluginManager->setText(tr("Plugin Config"));
    //
    if (actionRedo) {
        actionRedo->setText(tr("Redo"));
    }
    if (actionUndo) {
        actionUndo->setText(tr("Undo"));
    }
}
