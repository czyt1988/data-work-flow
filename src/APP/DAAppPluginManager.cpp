﻿#include "DAAppPluginManager.h"
#include "DAAbstractNodePlugin.h"
#include "DAAbstractPlugin.h"
#include "DAPluginManager.h"
#include "DAAbstractNodeFactory.h"

namespace DA
{
class _DAPrivateWorkflowNodePluginData
{
public:
    _DAPrivateWorkflowNodePluginData();
    ~_DAPrivateWorkflowNodePluginData();
    DAAbstractNodePlugin* plugin   = { nullptr };
    DAAbstractNodeFactory* factory = { nullptr };
};
}  // namespace DA
//===================================================
// using DA namespace -- 禁止在头文件using！！
//===================================================

using namespace DA;

//===================================================
// _DAPrivateNodePluginData
//===================================================

_DAPrivateWorkflowNodePluginData::_DAPrivateWorkflowNodePluginData()
{
}

_DAPrivateWorkflowNodePluginData::~_DAPrivateWorkflowNodePluginData()
{
    if (plugin && factory) {
        plugin->destoryNodeFactory(factory);
    }
}
//===================================================
// DAWorkFlowPluginManager
//===================================================
DAAppPluginManager::DAAppPluginManager(QObject* p) : QObject(p)
{
}

DAAppPluginManager::~DAAppPluginManager()
{
    for (_DAPrivateWorkflowNodePluginData* d : qAsConst(m_nodePlugins)) {
        delete d;
    }
}

DAAppPluginManager& DAAppPluginManager::instance()
{
    static DAAppPluginManager s_mgr;

    return (s_mgr);
}

/**
 * @brief 初始化加载所有的插件
 * @param 核心接口
 */
void DAAppPluginManager::initLoadPlugins(DACoreInterface* c)
{
    //加载插件
    DAPluginManager& plugin = DAPluginManager::instance();

    if (!plugin.isLoaded()) {
        plugin.load(c);
    }
    //获取插件
    QList< DAPluginOption > plugins = plugin.getPluginOptions();
    _nodeMetaDatas.clear();
    for (int i = 0; i < plugins.size(); ++i) {
        DAPluginOption opt = plugins[ i ];
        if (!opt.isValid()) {
            continue;
        }
        DAAbstractPlugin* p = opt.plugin();
        if (p) {
            //开始通过dynamic_cast判断插件的具体类型
            if (DAAbstractNodePlugin* np = dynamic_cast< DAAbstractNodePlugin* >(p)) {
                //说明是节点插件
                _DAPrivateWorkflowNodePluginData* data = new _DAPrivateWorkflowNodePluginData();
                data->plugin                           = np;
                data->factory                          = np->createNodeFactory();
                _nodeMetaDatas += data->factory->getNodesMetaData().toSet();
                m_nodePlugins.append(data);
                qDebug() << tr("succeed load plugin %1").arg(np->getName());
            }
        }
    }
}

/**
 * @brief 获取所有的节点插件
 * @return
 */
QList< DAAbstractNodePlugin* > DAAppPluginManager::getNodePlugins() const
{
    QList< DAAbstractNodePlugin* > res;

    for (_DAPrivateWorkflowNodePluginData* d : m_nodePlugins) {
        res.append(d->plugin);
    }
    return (res);
}

/**
 * @brief 获取所有的节点工厂
 * @return
 */
QList< DA::DAAbstractNodeFactory* > DAAppPluginManager::getNodeFactorys() const
{
    QList< DA::DAAbstractNodeFactory* > res;

    for (_DAPrivateWorkflowNodePluginData* d : m_nodePlugins) {
        res.append(d->factory);
    }
    return (res);
}

/**
 * @brief 获取所有的元数据
 * @return
 */
QList< DANodeMetaData > DAAppPluginManager::getAllNodeMetaDatas() const
{
    return _nodeMetaDatas.toList();
}
