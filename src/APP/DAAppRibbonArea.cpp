﻿#include "DAAppRibbonArea.h"
#include "AppMainWindow.h"
#include "ui_AppMainWindow.h"
// SARibbon
#include "SARibbonMainWindow.h"
#include "SARibbonBar.h"
#include "SARibbonButtonGroupWidget.h"
#include "SARibbonCategory.h"
#include "SARibbonPannel.h"
#include "SARibbonContextCategory.h"
#include "SARibbonQuickAccessBar.h"
#include "SARibbonLineWidgetContainer.h"
#include "SARibbonComboBox.h"
#include "SARibbonButtonGroupWidget.h"
#include "SARibbonMenu.h"
// stl
#include <memory>
// Qt
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include <QSignalBlocker>
#include <QStandardPaths>
#include <QFontComboBox>
#include <QComboBox>

#include <QInputDialog>
#include <QMenu>
// ui
#include "DAWaitCursorScoped.h"
// Py
#include "DAPyScripts.h"
#include "pandas/DAPyDataFrame.h"
#include "numpy/DAPyDType.h"
// api
#include "DAAppCommand.h"
#include "DAAppCore.h"
#include "DAAppDataManager.h"
#include "DADataManagerInterface.h"
#include "DAAppDockingAreaInterface.h"
#include "DAAppDockingArea.h"
#include "DAAppActions.h"
// Qt-Advanced-Docking-System
#include "DockManager.h"
#include "DockAreaWidget.h"
// command
#include "DACommandsDataManager.h"
// Widget
#include "DADataOperateWidget.h"
#include "DADataOperateOfDataFrameWidget.h"
#include "DAPyDTypeComboBox.h"
#include "DADataManageWidget.h"
// Dialog
#include "DAPluginManagerDialog.h"
#include "Dialog/DARenameColumnsNameDialog.h"
// DACommonWidgets
#include "DAFontEditPannelWidget.h"
#include "DAShapeEditPannelWidget.h"
// Workflow
#include "DAWorkFlowOperateWidget.h"
#include "DAWorkFlowGraphicsView.h"
#include "DADataWorkFlow.h"
// project
#include "DAProject.h"

#ifndef DAAPPRIBBONAREA_WINDOW_NAME
#define DAAPPRIBBONAREA_WINDOW_NAME QCoreApplication::translate("DAAppRibbonArea", "DA", nullptr)  //
#endif

//快速链接信号槽
#define DAAPPRIBBONAREA_ACTION_BIND(actionname, functionname)                                                          \
    connect(actionname, &QAction::triggered, this, &DAAppRibbonArea::functionname)

//未实现的功能标记
#define DAAPPRIBBONAREA_PASS()                                                                                         \
    QMessageBox::                                                                                                      \
    warning(app(),                                                                                                     \
            QCoreApplication::translate("DAAppRibbonArea", "warning", nullptr),                                        \
            QCoreApplication::translate("DAAppRibbonArea",                                                             \
                                        "The current function is not implemented, only the UI is reserved, "           \
                                        "please pay attention: https://gitee.com/czyt1988/data-work-flow",             \
                                        nullptr))

//===================================================
// using DA namespace -- 禁止在头文件using！！
//===================================================

using namespace DA;

//===================================================
// DAAppRibbonArea
//===================================================

DAAppRibbonArea::DAAppRibbonArea(DAAppUIInterface* u) : DAAppRibbonAreaInterface(u), m_dockArea(nullptr)
{
    m_app     = qobject_cast< AppMainWindow* >(u->mainWindow());
    m_datas   = qobject_cast< DAAppDataManager* >(u->core()->getDataManagerInterface());
    m_actions = qobject_cast< DAAppActions* >(u->getActionInterface());
    m_appCmd  = qobject_cast< DAAppCommand* >(u->getCommandInterface());
    //设置当前的焦点窗口
    m_lastFocusedOpertateWidget = LastFocusedNoneOptWidget;
    buildMenu();
    buildRibbon();
    initConnection();
    initScripts();
    resetText();
}

DAAppRibbonArea::~DAAppRibbonArea()
{
}

/**
 * @brief 构建所有的action
 */
void DAAppRibbonArea::buildMenu()
{
    m_menuInsertRow = new SARibbonMenu(m_app);
    m_menuInsertRow->setObjectName("menuInsertRow");
    m_menuInsertRow->addAction(m_actions->actionInsertRowAbove);
    m_menuInsertColumn = new SARibbonMenu(m_app);
    m_menuInsertColumn->setObjectName("menuInsertColumn");
    m_menuInsertColumn->addAction(m_actions->actionInsertColumnLeft);
}

void DAAppRibbonArea::retranslateUi()
{
    resetText();
}

void DAAppRibbonArea::resetText()
{
    ribbonBar()->applicationButton()->setText(tr("File"));  //文件

    m_categoryMain->setCategoryName(tr("Main"));               //主页
    m_pannelMainFileOpt->setPannelName(tr("File Operation"));  //文件操作
    m_pannelSetting->setPannelName(tr("Config"));              //配置
    m_pannelMainWorkflow->setPannelName(tr("Workflow"));       //工作流
    m_pannelMainDataOpt->setPannelName(tr("Data Operation"));  //文件操作
    m_categoryData->setCategoryName(tr("Data"));               //数据
    m_pannelDataOperate->setPannelName(tr("Data Operation"));  //数据操作

    m_categoryView->setCategoryName(tr("View"));         //视图
    m_pannelViewMainView->setPannelName(tr("Display"));  //视图显示

    m_contextDataFrame->setContextTitle(tr("DataFrame"));               ///< DataFrame
    m_categoryDataframeOperate->setCategoryName(tr("Operate"));         ///< DataFrame -> Operate
    m_pannelDataframeOperateAxes->setPannelName(tr("Axes"));            ///< DataFrame -> Operate -> Axes
    m_pannelDataframeOperateDType->setPannelName(tr("Type"));           ///< DataFrame -> Type
    m_comboxColumnTypesContainer->setPrefix(tr("Type"));                ///< DataFrame -> Type -> Type
    m_pannelDataframeOperateStatistic->setPannelName(tr("Statistic"));  ///< DataFrame -> Statistic

    //编辑标签
    m_categoryEdit->setCategoryName(tr("Edit"));  //编辑

    m_contextWorkflow->setContextTitle(tr("Workflow"));  //工作流

    m_categoryWorkflowGraphicsEdit->setCategoryName(tr("Edit"));  //编辑
    m_pannelWorkflowItem->setPannelName(tr("Item"));              //图元
    m_pannelWorkflowText->setPannelName(tr("Text"));              //文本
    m_pannelWorkflowBackground->setPannelName(tr("Background"));  //文本
    m_pannelWorkflowView->setPannelName(tr("View"));              //视图
    m_pannelWorkflowRun->setPannelName(tr("Run"));                //运行

    //
    m_app->setWindowTitle(tr("Data Work Flow"));
}

/**
 * @brief 基本绑定
 * @note 在setDockAreaInterface函数中还有很多绑定操作
 */
void DAAppRibbonArea::initConnection()
{
    // Main Category
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionOpen, onActionOpenTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionSave, onActionSaveTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionSaveAs, onActionSaveAsTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionAppendProject, onActionAppendProjectTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionSaveCurrentWorkflowToFixProject, onActionSaveCurrentWorkflowToFixProjectTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionSetting, onActionSettingTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionPluginManager, onActionPluginManagerTriggered);
    // Data Category
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionAddData, onActionAddDataTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionRemoveData, onActionRemoveDataTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionAddDataFolder, onActionAddDataFolderTriggered);
    // 数据操作的上下文标签 Data Operate Context Category
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionInsertRow, onActionInsertRowTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionInsertRowAbove, onActionInsertRowAboveTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionInsertColumnRight, onActionInsertColumnRightTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionInsertColumnLeft, onActionInsertColumnLeftTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionRemoveRow, onActionRemoveRowTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionRemoveColumn, onActionRemoveColumnTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionRemoveCell, onActionRemoveCellTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionRenameColumns, onActionRenameColumnsTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionCreateDataDescribe, onActionCreateDataDescribeTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionCastToNum, onActionCastToNumTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionCastToString, onActionCastToStringTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionCastToDatetime, onActionCastToDatetimeTriggered);
    //不知为何使用函数指针无法关联信号和槽
    // connect(m_comboxColumnTypes, &DAPyDTypeComboBox::currentDTypeChanged, this,&DAAppRibbonArea::onComboxColumnTypesCurrentDTypeChanged);
    // QObject::connect: signal not found in DAPyDTypeComboBox
    connect(m_comboxColumnTypes, SIGNAL(currentDTypeChanged(DAPyDType)), this, SLOT(onComboxColumnTypesCurrentDTypeChanged(DAPyDType)));
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionChangeToIndex, onActionChangeToIndexTriggered);
    // View Category
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionShowWorkFlowArea, onActionShowWorkFlowAreaTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionShowChartArea, onActionShowChartAreaTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionShowDataArea, onActionShowDataAreaTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionShowMessageLogView, onActionShowMessageLogViewTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionShowSettingWidget, onActionSettingWidgetTriggered);

    // workflow edit 工作流编辑
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionWorkflowNew, onActionNewWorkflowTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionWorkflowRun, onActionRunCurrentWorkflowTriggered);
    // workflow edit 工作流编辑/data edit 绘图编辑
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionWorkflowStartDrawRect, onActionStartDrawRectTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionWorkflowStartDrawText, onActionStartDrawTextTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionWorkflowAddBackgroundPixmap, onActionAddBackgroundPixmapTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionWorkflowLockBackgroundPixmap, onActionLockBackgroundPixmapTriggered);
    DAAPPRIBBONAREA_ACTION_BIND(m_actions->actionWorkflowEnableItemMoveWithBackground, onActionEnableItemMoveWithBackgroundTriggered);

    //===================================================
    // setDockAreaInterface 有其他的绑定
    //===================================================
    //! 注意！！
    //! 在setDockAreaInterface函数中还有很多绑定操作
    //
    DAProject* p = DA_APP_CORE.getProject();
    if (p) {
        connect(p, &DAProject::projectSaved, this, &DAAppRibbonArea::onProjectSaved);
        connect(p, &DAProject::projectLoaded, this, &DAAppRibbonArea::onProjectLoaded);
    }

    //===================================================
    // name
    //===================================================

    connect(m_workflowFontEditPannel, &DA::DAFontEditPannelWidget::currentFontChanged, this, &DAAppRibbonArea::onCurrentWorkflowFontChanged);
    connect(m_workflowFontEditPannel, &DA::DAFontEditPannelWidget::currentFontColorChanged, this, &DAAppRibbonArea::onCurrentWorkflowFontColorChanged);
    connect(m_workflowShapeEditPannelWidget, &DAShapeEditPannelWidget::backgroundBrushChanged, this, &DAAppRibbonArea::onCurrentWorkflowShapeBackgroundBrushChanged);
    connect(m_workflowShapeEditPannelWidget, &DAShapeEditPannelWidget::borderPenChanged, this, &DAAppRibbonArea::onCurrentWorkflowShapeBorderPenChanged);
}

/**
 * @brief 由于无法判断DAAppRibbonArea和DAAppDockingArea的创建顺序，因此DAAppDockingArea的指针手动设置进去
 * @param d
 */
void DAAppRibbonArea::setDockAreaInterface(DAAppDockingArea* d)
{
    m_dockArea = d;
    connect(d->dockManager(), &ads::CDockManager::focusedDockWidgetChanged, this, &DAAppRibbonArea::onFocusedDockWidgetChanged);
    //数据操作
    DADataManageWidget* dmw = m_dockArea->getDataManageWidget();
    connect(dmw, &DADataManageWidget::dataViewModeChanged, this, &DAAppRibbonArea::onDataManageWidgetDataViewModeChanged);
    DADataOperateWidget* dow = m_dockArea->getDataOperateWidget();
    connect(dow, &DADataOperateWidget::pageAdded, this, &DAAppRibbonArea::onDataOperatePageAdded);
    //鼠标动作完成的触发
    connect(m_dockArea->getWorkFlowOperateWidget(),
            &DAWorkFlowOperateWidget::mouseActionFinished,
            this,
            &DAAppRibbonArea::onWorkFlowGraphicsSceneMouseActionFinished);
    //
    DAWorkFlowOperateWidget* workflowOpt = m_dockArea->getWorkFlowOperateWidget();
    connect(workflowOpt, &DAWorkFlowOperateWidget::selectionItemChanged, this, &DAAppRibbonArea::onSelectionItemChanged);
    connect(m_actions->actionWorkflowShowGrid, &QAction::triggered, workflowOpt, &DAWorkFlowOperateWidget::setCurrentWorkflowShowGrid);
    connect(m_actions->actionWorkflowNew, &QAction::triggered, workflowOpt, &DAWorkFlowOperateWidget::appendWorkflowWithDialog);
    connect(m_actions->actionWorkflowWholeView, &QAction::triggered, workflowOpt, &DAWorkFlowOperateWidget::setCurrentWorkflowWholeView);
    connect(m_actions->actionWorkflowZoomIn, &QAction::triggered, workflowOpt, &DAWorkFlowOperateWidget::setCurrentWorkflowZoomIn);
    connect(m_actions->actionWorkflowZoomOut, &QAction::triggered, workflowOpt, &DAWorkFlowOperateWidget::setCurrentWorkflowZoomOut);
}

/**
 * @brief 构建ribbon
 */
void DAAppRibbonArea::buildRibbon()
{
    buildRibbonMainCategory();
    buildRibbonDataCategory();
    buildRibbonViewCategory();
    buildRibbonEditCategory();
    buildRibbonQuickAccessBar();
    //上下文标签
    buildContextCategoryDataFrame();
    buildContextCategoryWorkflowEdit();
}

/**
 * @brief 构建主页标签
 * 主页的category objname = da-ribbon-category-main
 */
void DAAppRibbonArea::buildRibbonMainCategory()
{
    m_categoryMain = new SARibbonCategory(app());
    m_categoryMain->setObjectName(QStringLiteral("da-ribbon-category-main"));

    //--------Common--------------------------------------------------

    m_pannelMainFileOpt = new SARibbonPannel(m_categoryMain);
    m_pannelMainFileOpt->setObjectName(QStringLiteral("pannelCommon"));
    m_pannelMainFileOpt->addLargeAction(m_actions->actionOpen);
    m_pannelMainFileOpt->addSmallAction(m_actions->actionSave);
    m_pannelMainFileOpt->addSmallAction(m_actions->actionSaveAs);
    m_pannelMainFileOpt->addSeparator();
    m_pannelMainFileOpt->addLargeAction(m_actions->actionAppendProject);
    m_pannelMainFileOpt->addSeparator();
    m_pannelMainFileOpt->addLargeAction(m_actions->actionSaveCurrentWorkflowToFixProject);
    m_categoryMain->addPannel(m_pannelMainFileOpt);

    //--------Data Opt--------------------------------------------------

    m_pannelMainDataOpt = new SARibbonPannel(m_categoryMain);
    m_pannelMainDataOpt->setObjectName(QStringLiteral("pannelMainDataOpt"));
    m_pannelMainDataOpt->addLargeAction(m_actions->actionAddData);
    m_categoryMain->addPannel(m_pannelMainDataOpt);

    //--------Workflow Opt----------------------------------------------
    m_pannelMainWorkflow = m_categoryMain->addPannel(tr("Workflow"));
    m_pannelMainWorkflow->setObjectName(QStringLiteral("pannelMainWorkflow"));
    m_pannelMainWorkflow->addLargeAction(m_actions->actionWorkflowAddBackgroundPixmap);
    m_pannelMainWorkflow->addMediumAction(m_actions->actionWorkflowStartDrawRect);
    m_pannelMainWorkflow->addMediumAction(m_actions->actionWorkflowStartDrawText);
    m_pannelMainWorkflow->addLargeAction(m_actions->actionWorkflowRun);

    //--------Setting--------------------------------------------------

    m_pannelSetting = new SARibbonPannel(m_categoryMain);
    m_pannelSetting->setObjectName(QStringLiteral("pannelSetting"));
    m_pannelSetting->addLargeAction(m_actions->actionSetting);
    m_pannelSetting->addLargeAction(m_actions->actionPluginManager);
    m_categoryMain->addPannel(m_pannelSetting);
    //----------------------------------------------------------

    ribbonBar()->addCategoryPage(m_categoryMain);  //主页
}

/**
 * @brief 构建数据标签
 * objectname=da-ribbon-category-data
 */
void DAAppRibbonArea::buildRibbonDataCategory()
{
    m_categoryData = new SARibbonCategory(app());
    m_categoryData->setObjectName(QStringLiteral("da-ribbon-category-data"));

    //--------DataOperate--------------------------------------------------

    m_pannelDataOperate = new SARibbonPannel(m_categoryData);
    m_pannelDataOperate->setObjectName(QStringLiteral("pannelDataOperate"));
    m_pannelDataOperate->addLargeAction(m_actions->actionAddData);
    m_pannelDataOperate->addLargeAction(m_actions->actionRemoveData);
    m_categoryData->addPannel(m_pannelDataOperate);

    //--------FolderOperate--------------------------------------------------

    m_pannelDataFolderOperate = new SARibbonPannel(m_categoryData);
    m_pannelDataFolderOperate->setObjectName(QStringLiteral("pannelDataFolderOperate"));
    m_pannelDataFolderOperate->addLargeAction(m_actions->actionAddDataFolder);
    m_actions->actionAddDataFolder->setDisabled(true);  //默认不可点
    m_categoryData->addPannel(m_pannelDataFolderOperate);

    //----------------------------------------------------------

    ribbonBar()->addCategoryPage(m_categoryData);
}

/**
 * @brief 构建视图标签
 * objectname=da-ribbon-category-view
 */
void DAAppRibbonArea::buildRibbonViewCategory()
{
    m_categoryView = new SARibbonCategory(app());
    m_categoryView->setObjectName(QStringLiteral("da-ribbon-category-view"));

    //--------MainView--------------------------------------------------

    m_pannelViewMainView = new SARibbonPannel(m_categoryView);
    m_pannelViewMainView->setObjectName(QStringLiteral("pannelViewMainView"));
    m_pannelViewMainView->addLargeAction(m_actions->actionShowWorkFlowArea);
    m_pannelViewMainView->addLargeAction(m_actions->actionShowChartArea);
    m_pannelViewMainView->addLargeAction(m_actions->actionShowDataArea);
    m_pannelViewMainView->addSeparator();
    m_pannelViewMainView->addSmallAction(m_actions->actionShowMessageLogView);
    m_pannelViewMainView->addSmallAction(m_actions->actionShowSettingWidget);
    m_categoryView->addPannel(m_pannelViewMainView);

    //----------------------------------------------------------

    ribbonBar()->addCategoryPage(m_categoryView);  //视图
}

/**
 * @brief 构建ribbon的QuickAccessBar
 */
void DAAppRibbonArea::buildRibbonQuickAccessBar()
{
}

/**
 * @brief 构建DataFrame上下文标签
 * objectname=da-ribbon-contextcategory-dataframe
 */
void DAAppRibbonArea::buildContextCategoryDataFrame()
{
    m_contextDataFrame = ribbonBar()->addContextCategory(tr("DataFrame"));
    m_contextDataFrame->setObjectName(QStringLiteral("da-ribbon-contextcategory-dataframe"));
    m_categoryDataframeOperate = m_contextDataFrame->addCategoryPage(tr("Operate"));
    // Axes pannel
    m_pannelDataframeOperateAxes = m_categoryDataframeOperate->addPannel(tr("Axes"));
    m_pannelDataframeOperateAxes->addLargeActionMenu(m_actions->actionInsertRow, m_menuInsertRow);
    m_pannelDataframeOperateAxes->addLargeActionMenu(m_actions->actionInsertColumnRight, m_menuInsertColumn);
    m_pannelDataframeOperateAxes->addLargeAction(m_actions->actionRemoveCell);
    m_pannelDataframeOperateAxes->addMediumAction(m_actions->actionRemoveRow);
    m_pannelDataframeOperateAxes->addMediumAction(m_actions->actionRemoveColumn);
    m_pannelDataframeOperateAxes->addSeparator();
    m_pannelDataframeOperateAxes->addLargeAction(m_actions->actionRenameColumns);
    m_pannelDataframeOperateAxes->addLargeAction(m_actions->actionChangeToIndex);
    // Type pannel
    m_pannelDataframeOperateDType = m_categoryDataframeOperate->addPannel(tr("Type"));
    m_comboxColumnTypesContainer  = new SARibbonLineWidgetContainer(m_pannelDataframeOperateDType);
    m_comboxColumnTypes           = new DAPyDTypeComboBox(m_comboxColumnTypesContainer);
    m_comboxColumnTypes->setMinimumWidth(m_app->fontMetrics().width("timedelta64(scoll)"));  //设置最小宽度
    m_comboxColumnTypesContainer->setPrefix(tr("Type"));
    m_comboxColumnTypesContainer->setWidget(m_comboxColumnTypes);
    m_pannelDataframeOperateDType->addWidget(m_comboxColumnTypesContainer, SARibbonPannelItem::Medium);
    m_castActionsButtonGroup = new SARibbonButtonGroupWidget();
    m_castActionsButtonGroup->addAction(m_actions->actionCastToNum);
    m_castActionsButtonGroup->addAction(m_actions->actionCastToString);
    m_castActionsButtonGroup->addSeparator();
    m_castActionsButtonGroup->addAction(m_actions->actionCastToDatetime);
    m_pannelDataframeOperateDType->addWidget(m_castActionsButtonGroup, SARibbonPannelItem::Medium);
    // Statistic Pannel
    m_pannelDataframeOperateStatistic = m_categoryDataframeOperate->addPannel(tr("Statistic"));
    m_pannelDataframeOperateStatistic->addLargeAction(m_actions->actionCreateDataDescribe);
}

/**
 * @brief 构建Edit标签
 * da-ribbon-category-edit
 * pannel:da-ribbon-pannel-edit-workflow
 */

void DAAppRibbonArea::buildRibbonEditCategory()
{
    m_categoryEdit = new SARibbonCategory(app());
    m_categoryEdit->setObjectName(QStringLiteral("da-ribbon-category-edit"));

    //--------MainView--------------------------------------------------

    m_pannelEditWorkflow = new SARibbonPannel(m_categoryEdit);
    m_pannelEditWorkflow->setObjectName(QStringLiteral("da-ribbon-pannel-edit-workflow"));
    m_pannelEditWorkflow->addLargeAction(m_actions->actionWorkflowNew);
    m_pannelEditWorkflow->addSeparator();
    m_pannelEditWorkflow->addLargeAction(m_actions->actionWorkflowStartDrawRect);
    m_pannelEditWorkflow->addLargeAction(m_actions->actionWorkflowStartDrawText);
    m_categoryEdit->addPannel(m_pannelEditWorkflow);
    //----------------------------------------------------------

    ribbonBar()->addCategoryPage(m_categoryEdit);  //编辑
}

void DAAppRibbonArea::buildContextCategoryWorkflowEdit()
{
    m_contextWorkflow              = ribbonBar()->addContextCategory(tr("Workflow"));
    m_categoryWorkflowGraphicsEdit = m_contextWorkflow->addCategoryPage(tr("Edit"));
    //条目pannel
    // Item
    m_pannelWorkflowItem = m_categoryWorkflowGraphicsEdit->addPannel(tr("Item"));
    m_pannelWorkflowItem->addLargeAction(m_actions->actionWorkflowNew);
    m_pannelWorkflowItem->addSeparator();
    m_pannelWorkflowItem->addLargeAction(m_actions->actionWorkflowStartDrawRect);
    m_workflowShapeEditPannelWidget = new DAShapeEditPannelWidget(m_pannelWorkflowItem);
    m_pannelWorkflowItem->addWidget(m_workflowShapeEditPannelWidget, SARibbonPannelItem::Large);
    // Text
    m_pannelWorkflowText = m_categoryWorkflowGraphicsEdit->addPannel(tr("Text"));
    m_pannelWorkflowText->addLargeAction(m_actions->actionWorkflowStartDrawText);
    m_workflowFontEditPannel = new DAFontEditPannelWidget(m_pannelWorkflowText);
    m_pannelWorkflowText->addWidget(m_workflowFontEditPannel, SARibbonPannelItem::Large);
    // Background
    m_pannelWorkflowBackground = m_categoryWorkflowGraphicsEdit->addPannel(tr("Background"));
    m_pannelWorkflowBackground->addLargeAction(m_actions->actionWorkflowAddBackgroundPixmap);
    m_pannelWorkflowBackground->addMediumAction(m_actions->actionWorkflowLockBackgroundPixmap);
    m_pannelWorkflowBackground->addMediumAction(m_actions->actionWorkflowEnableItemMoveWithBackground);

    // View
    m_pannelWorkflowView = m_categoryWorkflowGraphicsEdit->addPannel(tr("View"));
    m_pannelWorkflowView->addLargeAction(m_actions->actionWorkflowShowGrid);
    m_pannelWorkflowView->addSeparator();
    m_pannelWorkflowView->addLargeAction(m_actions->actionWorkflowWholeView);
    m_pannelWorkflowView->addMediumAction(m_actions->actionWorkflowZoomIn);
    m_pannelWorkflowView->addMediumAction(m_actions->actionWorkflowZoomOut);
    // Run
    m_pannelWorkflowRun = m_categoryWorkflowGraphicsEdit->addPannel(tr("Run"));
    m_pannelWorkflowRun->addLargeAction(m_actions->actionWorkflowRun);
    ribbonBar()->showContextCategory(m_contextWorkflow);
}

/**
 * @brief 脚本定义的内容初始化
 */
void DAAppRibbonArea::initScripts()
{
    DAAppCore* c = qobject_cast< DAAppCore* >(core());
    if (!c->isPythonInterpreterInitialized()) {
        return;
    }
    m_fileReadFilters = QStringList(DAPyScripts::getInstance().getIO().getFileReadFilters());
    qDebug() << m_fileReadFilters;
}

/**
 * @brief 判断当前是否是在绘图操作模式，就算绘图操作不在焦点，但绘图操作在前端，此函数也返回true
 * @return
 */
bool DAAppRibbonArea::isLastFocusedOnChartOptWidget() const
{
    return m_lastFocusedOpertateWidget.testFlag(LastFocusedOnChartOpt);
}
/**
 * @brief 判断当前是否是在工作流操作模式，就算工作流操作不在焦点，但工作流操作在前端，此函数也返回true
 * @return
 */
bool DAAppRibbonArea::isLastFocusedOnWorkflowOptWidget() const
{
    return m_lastFocusedOpertateWidget.testFlag(LastFocusedOnWorkflowOpt);
}
/**
 * @brief 判断当前是否是在数据操作模式，就算数据操作不在焦点，但工作流操作在前端，此函数也返回true
 * @return
 */
bool DAAppRibbonArea::isLastFocusedOnDataOptWidget() const
{
    return m_lastFocusedOpertateWidget.testFlag(LastFocusedOnDataOpt);
}

/**
 * @brief DADataManageWidget查看数据的模式改变
 * @param v
 */
void DAAppRibbonArea::onDataManageWidgetDataViewModeChanged(DADataManageWidget::DataViewMode v)
{
    m_actions->actionAddDataFolder->setEnabled(v == DADataManageWidget::ViewDataInTree);
}

/**
 * @brief GraphicsScene的鼠标动作执行完成，把action的选中标记清除
 * @param mf
 */
void DAAppRibbonArea::onWorkFlowGraphicsSceneMouseActionFinished(DAWorkFlowGraphicsScene::MouseActionFlag mf)
{
    switch (mf) {
    case DAWorkFlowGraphicsScene::StartAddRect:
        m_actions->actionWorkflowStartDrawRect->setChecked(false);
        break;
    case DAWorkFlowGraphicsScene::StartAddText:
        m_actions->actionWorkflowStartDrawText->setChecked(false);
        break;
    default:
        break;
    }
}

AppMainWindow* DAAppRibbonArea::app() const
{
    return (m_app);
}

SARibbonBar* DAAppRibbonArea::ribbonBar() const
{
    return (app()->ribbonBar());
}

/**
 * @brief mian标签
 * @return
 */
SARibbonCategory* DAAppRibbonArea::getRibbonCategoryMain() const
{
    return (m_categoryMain);
}

QStringList DAAppRibbonArea::getFileReadFilters() const
{
    return m_fileReadFilters;
}

/**
 * @brief 通过DACommandInterface构建redo/undo的action
 * @param cmd
 */
void DAAppRibbonArea::buildRedoUndo(DACommandInterface* cmd)
{
    DAAppCommand* cmdapi = qobject_cast< DAAppCommand* >(cmd);
    Q_ASSERT_X(cmdapi != nullptr, "DAAppRibbonArea::buildRedoUndo", "DACommandInterface* can not cast to DAAppCommand*");
    QUndoGroup& undoGroup = cmdapi->undoGroup();
    //设置redo,undo的action
    m_actions->actionRedo = undoGroup.createRedoAction(this);
    m_actions->actionRedo->setObjectName("actionRedo");
    m_actions->actionRedo->setIcon(QIcon(":/Icon/Icon/redo.svg"));
    m_actions->actionUndo = undoGroup.createUndoAction(this);
    m_actions->actionUndo->setObjectName("actionUndo");
    m_actions->actionUndo->setIcon(QIcon(":/Icon/Icon/undo.svg"));
    SARibbonQuickAccessBar* bar = ribbonBar()->quickAccessBar();
    if (!bar) {
        return;
    }
    bar->addAction(m_actions->actionUndo);
    bar->addAction(m_actions->actionRedo);
}

void DAAppRibbonArea::updateActionLockBackgroundPixmapCheckStatue(bool c)
{
    //    QSignalBlocker l(m_actionLockBackgroundPixmap);
    //    m_actionLockBackgroundPixmap->setChecked(c);
}

/**
 * @brief 插件管理 [config category] - [Plugin Manager]
 * @param on
 */
void DAAppRibbonArea::onActionPluginManagerTriggered(bool on)
{
    Q_UNUSED(on);
    DAPluginManagerDialog dlg(ui()->mainWindow());

    dlg.exec();
}

/**
 * @brief 设定界面
 */
void DAAppRibbonArea::onActionSettingTriggered()
{
    DAAPPRIBBONAREA_PASS();
}

/**
 * @brief 获取当前dataframeOperateWidget,如果没有返回nullptr
 *
 * 此函数不返回nullptr的前提是:DataOperateWidget处于焦点，且是DataFrameOperateWidget
 * @param checkDataOperateAreaFocused 是否检测DataOperateWidget是否处于焦点，默认为true
 * @return
 */
DADataOperateOfDataFrameWidget* DAAppRibbonArea::getCurrentDataFrameOperateWidget(bool checkDataOperateAreaFocused)
{
    if (nullptr == m_dockArea) {
        return nullptr;
    }
    if (checkDataOperateAreaFocused) {
        if (!(m_dockArea->isDockingAreaFocused(DAAppDockingArea::DockingAreaDataOperate))) {
            //窗口未选中就退出
            return nullptr;
        }
    }
    return m_dockArea->getDataOperateWidget()->currentDataFrameWidget();
}

/**
 * @brief 设置DataFrame的类型，[Context Category - dataframe] [Type] -> Type
 * @param d
 */
void DAAppRibbonArea::setDataframeOperateCurrentDType(const DAPyDType& d)
{
    //先阻塞
    QSignalBlocker blocker(m_comboxColumnTypes);
    Q_UNUSED(blocker);
    m_comboxColumnTypes->setCurrentDType(d);
}

/**
 * @brief DockWidget的焦点变化
 * @param old
 * @param now
 */
void DAAppRibbonArea::onFocusedDockWidgetChanged(ads::CDockWidget* old, ads::CDockWidget* now)
{
    Q_UNUSED(old);
    SARibbonBar* ribbon = ribbonBar();
    if (nullptr == now) {
        ribbon->hideContextCategory(m_contextDataFrame);
        ribbon->hideContextCategory(m_contextWorkflow);
        return;
    }
    //数据操作窗口激活时，检查是否需要显示m_contextDataFrame
    if ((QWidget*)(m_dockArea->getDataOperateWidget()) == now->widget()) {
        //数据窗口激活
        ribbon->showContextCategory(m_contextDataFrame);
        ribbon->hideContextCategory(m_contextWorkflow);
        m_lastFocusedOpertateWidget = LastFocusedOnDataOpt;
    } else if (now->widget() == (QWidget*)(m_dockArea->getWorkFlowOperateWidget())) {
        m_dockArea->getWorkFlowOperateWidget()->setUndoStackActive();
        ribbon->showContextCategory(m_contextWorkflow);
        ribbon->hideContextCategory(m_contextDataFrame);
        m_lastFocusedOpertateWidget = LastFocusedOnWorkflowOpt;
    } else if (now->widget() == (QWidget*)(m_dockArea->getDataOperateWidget())) {
        m_lastFocusedOpertateWidget = LastFocusedOnDataOpt;
    } else if ((QWidget*)(m_dockArea->getDataManageWidget()) == now->widget()) {
        if (m_appCmd) {
            QUndoStack* stack = m_appCmd->getDataManagerStack();
            if (stack && !(stack->isActive())) {  // Data 相关的窗口 undostack激活
                stack->setActive();
            }
        }
    }
}
/**
 * @brief 打开文件
 */
void DAAppRibbonArea::onActionOpenTriggered()
{
    // TODO : 这里要加上工程文件的打开支持
    QFileDialog dialog(app());
    QStringList filters;
    filters << tr("project file(*.%1)").arg(DAProject::getProjectFileSuffix());
    dialog.setNameFilters(filters);
    if (QDialog::Accepted != dialog.exec()) {
        return;
    }
    QStringList fileNames = dialog.selectedFiles();
    if (fileNames.empty()) {
        return;
    }
    DAProject* project = DA_APP_CORE.getProject();
    if (!project->getProjectDir().isEmpty()) {
        if (project->isDirty()) {
            // TODO 没有保存。先询问是否保存
            QMessageBox::StandardButton
            btn = QMessageBox::question(nullptr,
                                        tr("Question"),                                                   //提示
                                        tr("Another project already exists. Do you want to replace it?")  //已存在其他工程，是否要替换？
            );
            if (btn == QMessageBox::Yes) {
                project->clear();
            } else {
                return;
            }
        } else {
            project->clear();
        }
    }
    DA_WAIT_CURSOR_SCOPED();

    if (!project->load(fileNames.first())) {
        qCritical() << tr("failed to load project file:%1").arg(fileNames.first());
        return;
    }
    //设置工程名称给标题
    app()->setWindowTitle(QString("%1").arg(project->getProjectBaseName()));
}

/**
 * @brief 另存为
 */
void DAAppRibbonArea::onActionSaveAsTriggered()
{
    QString projectPath = QFileDialog::getSaveFileName(app(),
                                                       tr("Save Project"),  //保存工程
                                                       QString(),
                                                       tr("project file (*.%1)").arg(DAProject::getProjectFileSuffix())  // 工程文件
    );
    if (projectPath.isEmpty()) {
        //取消退出
        return;
    }
    QFileInfo fi(projectPath);
    if (fi.exists()) {
        //说明是目录
        QMessageBox::StandardButton btn = QMessageBox::question(nullptr,
                                                                tr("Warning"),
                                                                tr("Whether to overwrite the file:%1").arg(fi.absoluteFilePath()));
        if (btn != QMessageBox::Yes) {
            return;
        }
    }
    //另存为
    DA_WAIT_CURSOR_SCOPED();
    DAProject* project = DA_APP_CORE.getProject();
    if (!project->save(projectPath)) {
        qCritical() << tr("Project saved failed!,path is %1").arg(projectPath);  //工程保存失败！路径位于:%1
        return;
    }
    app()->setWindowTitle(QString("%1").arg(project->getProjectBaseName()));
    qInfo() << tr("Project saved successfully,path is %1").arg(projectPath);  //工程保存成功，路径位于:%1
}

void DAAppRibbonArea::onActionAppendProjectTriggered()
{
    QFileDialog dialog(app());
    QStringList filters;
    filters << tr("project file(*.%1)").arg(DAProject::getProjectFileSuffix());
    dialog.setNameFilters(filters);
    if (QDialog::Accepted != dialog.exec()) {
        return;
    }
    QStringList fileNames = dialog.selectedFiles();
    if (fileNames.empty()) {
        return;
    }
    DAProject* project = DA_APP_CORE.getProject();
    DA_WAIT_CURSOR_SCOPED();

    if (!project->appendProjectWorkflow(fileNames.first(), true)) {
        qCritical() << tr("failed to load project file:%1").arg(fileNames.first());
        return;
    }
    //设置工程名称给标题
    if (project->getProjectBaseName().isEmpty()) {
        app()->setWindowTitle(QString("untitle"));
    } else {
        app()->setWindowTitle(QString("%1").arg(project->getProjectBaseName()));
    }
}

/**
 * @brief 把已有工作流保存到已有方案中
 */
void DAAppRibbonArea::onActionSaveCurrentWorkflowToFixProjectTriggered()
{
    DAWorkFlowOperateWidget* wf = m_dockArea->getWorkFlowOperateWidget();
    int index                   = wf->getCurrentWorkflowIndex();
    if (index < 0) {
        return;
    }
    DAWorkFlowEditWidget* currentWf = wf->getWorkFlowWidget(index);
    if (nullptr == currentWf) {
        return;
    }
    bool ok      = false;
    QString text = QInputDialog::getText(app(), tr("新方案名称"), tr("新方案名称:"), QLineEdit::Normal, wf->getWorkFlowWidgetName(index), &ok);
    if (!ok || text.isEmpty()) {
        return;
    }
    DAProject* project = DA_APP_CORE.getProject();
    // TODO
}

/**
 * @brief 保存工程
 */
void DAAppRibbonArea::onActionSaveTriggered()
{
    DAProject* project      = DA_APP_CORE.getProject();
    QString projectFilePath = project->getProjectFilePath();
    qDebug() << "Save Project,Path=" << projectFilePath;
    if (projectFilePath.isEmpty()) {
        QString desktop = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
        projectFilePath = QFileDialog::getSaveFileName(nullptr,
                                                       tr("Save Project"),  //保存工程
                                                       desktop,
                                                       tr("Air System Project Files (*.%1)").arg(DAProject::getProjectFileSuffix())  // ASS文件 (*.asproj)
        );
        if (projectFilePath.isEmpty()) {
            //取消退出
            return;
        }
    }
    bool saveRet = project->save(projectFilePath);
    if (!saveRet) {
        qCritical() << tr("Project saved failed!,path is %1").arg(projectFilePath);  //工程保存失败！路径位于:%1
    }
}

/**
 * @brief 工程成功保存触发的信号
 * @param path
 */
void DAAppRibbonArea::onProjectSaved(const QString& path)
{
    DAProject* project = DA_APP_CORE.getProject();
    m_app->setWindowTitle(QString("%1-%2").arg(DAAPPRIBBONAREA_WINDOW_NAME, project->getProjectBaseName()));
    if (m_dockArea) {
        DAWorkFlowOperateWidget* wf = m_dockArea->getWorkFlowOperateWidget();
        if (wf) {
            wf->setCurrentWorkflowName(project->getProjectBaseName());
        }
    }
    qInfo() << tr("Project saved successfully,path is %1").arg(path);  //工程保存成功，路径位于:%1
}

/**
 * @brief 工程成功加载触发的信号
 * @param path
 */
void DAAppRibbonArea::onProjectLoaded(const QString& path)
{
    DAProject* project = DA_APP_CORE.getProject();
    m_app->setWindowTitle(QString("%1-%2").arg(DAAPPRIBBONAREA_WINDOW_NAME, project->getProjectBaseName()));
    if (m_dockArea) {
        DAWorkFlowOperateWidget* wf = m_dockArea->getWorkFlowOperateWidget();
        if (wf) {
            wf->setCurrentWorkflowName(project->getProjectBaseName());
        }
    }
    qInfo() << tr("Project load successfully,path is %1").arg(path);  //工程保存成功，路径位于:%1
}

/**
 * @brief 数据操作窗口添加，需要绑定相关信号槽到ribbon的页面
 * @param page
 */
void DAAppRibbonArea::onDataOperatePageAdded(DADataOperatePageWidget* page)
{
    switch (page->getDataOperatePageType()) {
    case DADataOperatePageWidget::DataOperateOfDataFrame: {
        DADataOperateOfDataFrameWidget* w = static_cast< DADataOperateOfDataFrameWidget* >(page);
        connect(w, &DADataOperateOfDataFrameWidget::selectTypeChanged, this, &DAAppRibbonArea::onDataOperateDataFrameWidgetSelectTypeChanged);
    } break;
    default:
        break;
    }
}

/**
 * @brief 选择的样式改变信号
 * @param column
 * @param dt
 */
void DAAppRibbonArea::onDataOperateDataFrameWidgetSelectTypeChanged(const QList< int >& column, DAPyDType dt)
{
    Q_UNUSED(column);
    setDataframeOperateCurrentDType(dt);
}

/**
 * @brief 添加背景图
 */
void DAAppRibbonArea::onActionAddBackgroundPixmapTriggered()
{
    QStringList filters;
    filters << tr("Image files (*.png *.jpg)")  //图片文件 (*.png *.jpg)
            << tr("Any files (*)")              //任意文件 (*)
    ;

    QFileDialog dialog(app());
    dialog.setNameFilters(filters);

    if (QDialog::Accepted != dialog.exec()) {
        return;
    }
    QStringList f = dialog.selectedFiles();
    if (!f.isEmpty()) {
        DAWorkFlowOperateWidget* ow = m_dockArea->getWorkFlowOperateWidget();
        ow->addBackgroundPixmap(f.first());
        m_dockArea->raiseDockingArea(DAAppDockingArea::DockingAreaWorkFlowOperate);
    }
}

void DAAppRibbonArea::onActionLockBackgroundPixmapTriggered(bool on)
{
    m_dockArea->getWorkFlowOperateWidget()->setBackgroundPixmapLock(on);
}

void DAAppRibbonArea::onActionEnableItemMoveWithBackgroundTriggered(bool on)
{
    m_dockArea->getWorkFlowOperateWidget()->getCurrentWorkFlowScene()->enableItemMoveWithBackground(on);
}

void DAAppRibbonArea::onActionRunCurrentWorkflowTriggered()
{
    qDebug() << "onActionRunTriggered";
    //先检查是否有工程
    DAProject* p = DA_APP_CORE.getProject();
    if (nullptr == p) {
        qCritical() << tr("get null project");  // cn:空工程，接口异常
        return;
    }
    QString bn = p->getProjectBaseName();
    if (bn.isEmpty()) {
        QMessageBox::warning(app(),
                             tr("warning"),                                                   // cn:警告
                             tr("Before running the workflow, you need to save the project")  // cn：在运行工作流之前，需要先保存工程
        );
        return;
    }
    m_dockArea->getWorkFlowOperateWidget()->runCurrentWorkFlow();
}

void DAAppRibbonArea::onCurrentWorkflowFontChanged(const QFont& f)
{
    DAWorkFlowOperateWidget* wf = m_dockArea->getWorkFlowOperateWidget();
    wf->setDefaultTextFont(f);
    wf->setSelectTextFont(f);
}

void DAAppRibbonArea::onCurrentWorkflowFontColorChanged(const QColor& c)
{
    DAWorkFlowOperateWidget* wf = m_dockArea->getWorkFlowOperateWidget();
    wf->setDefaultTextColor(c);
    wf->setSelectTextColor(c);
}

void DAAppRibbonArea::onCurrentWorkflowShapeBackgroundBrushChanged(const QBrush& b)
{
    DAWorkFlowOperateWidget* wf = m_dockArea->getWorkFlowOperateWidget();
    wf->setSelectShapeBackgroundBrush(b);
}

void DAAppRibbonArea::onCurrentWorkflowShapeBorderPenChanged(const QPen& p)
{
    DAWorkFlowOperateWidget* wf = m_dockArea->getWorkFlowOperateWidget();
    wf->setSelectShapeBorderPen(p);
}

void DAAppRibbonArea::onSelectionItemChanged(QGraphicsItem* lastSelectItem)
{
    if (lastSelectItem == nullptr) {
        return;
    }

    if (DAGraphicsItem* daitem = dynamic_cast< DAGraphicsItem* >(lastSelectItem)) {
        //属于DAGraphicsItem系列
        QSignalBlocker b(m_workflowShapeEditPannelWidget);
        m_workflowShapeEditPannelWidget->setBackgroundBrush(daitem->getBackgroundBrush());
        m_workflowShapeEditPannelWidget->setBorderPen(daitem->getBorderPen());
    } else if (DAStandardGraphicsTextItem* titem = dynamic_cast< DAStandardGraphicsTextItem* >(lastSelectItem)) {
        m_workflowFontEditPannel->setCurrentFontColor(titem->defaultTextColor());
        m_workflowFontEditPannel->setCurrentFont(titem->font());
    }
}

/**
 * @brief 添加数据
 */
void DAAppRibbonArea::onActionAddDataTriggered()
{
    QFileDialog dialog(app());
    dialog.setNameFilters(getFileReadFilters());
    if (QDialog::Accepted != dialog.exec()) {
        return;
    }
    QStringList fileNames = dialog.selectedFiles();
    if (fileNames.empty()) {
        return;
    }
    DA_WAIT_CURSOR_SCOPED();
    int importdataCount = m_datas->importFromFiles(fileNames);
    if (importdataCount > 0) {
        m_dockArea->raiseDockByWidget((QWidget*)(m_dockArea->getDataManageWidget()));
    }
}

/**
 * @brief 移除数据
 */
void DAAppRibbonArea::onActionRemoveDataTriggered()
{
    DADataManageWidget* dmw = m_dockArea->getDataManageWidget();
    dmw->removeSelectData();
}

/**
 * @brief 添加数据文件夹
 */
void DAAppRibbonArea::onActionAddDataFolderTriggered()
{
    DADataManageWidget* dmw = m_dockArea->getDataManageWidget();
    dmw->addDataFolder();
}
/**
 * @brief dataframe删除行
 */
void DAAppRibbonArea::onActionRemoveRowTriggered()
{
    if (DADataOperateOfDataFrameWidget* dfopt = getCurrentDataFrameOperateWidget()) {
        dfopt->removeSelectRow();
    }
}

/**
 * @brief dataframe删除列
 */
void DAAppRibbonArea::onActionRemoveColumnTriggered()
{
    if (DADataOperateOfDataFrameWidget* dfopt = getCurrentDataFrameOperateWidget()) {
        dfopt->removeSelectColumn();
    }
}

/**
 * @brief 移除单元格
 */
void DAAppRibbonArea::onActionRemoveCellTriggered()
{
    if (DADataOperateOfDataFrameWidget* dfopt = getCurrentDataFrameOperateWidget()) {
        dfopt->removeSelectCell();
    }
}

/**
 * @brief 插入行
 */
void DAAppRibbonArea::onActionInsertRowTriggered()
{
    if (DADataOperateOfDataFrameWidget* dfopt = getCurrentDataFrameOperateWidget()) {
        dfopt->insertRowBelowBySelect();
    }
}

/**
 * @brief 在选中位置上面插入一行
 */
void DAAppRibbonArea::onActionInsertRowAboveTriggered()
{
    if (DADataOperateOfDataFrameWidget* dfopt = getCurrentDataFrameOperateWidget()) {
        dfopt->insertRowAboveBySelect();
    }
}
/**
 * @brief 在选中位置右边插入一列
 */
void DAAppRibbonArea::onActionInsertColumnRightTriggered()
{
    if (DADataOperateOfDataFrameWidget* dfopt = getCurrentDataFrameOperateWidget()) {
        dfopt->insertColumnRightBySelect();
    }
}
/**
 * @brief 在选中位置左边插入一列
 */
void DAAppRibbonArea::onActionInsertColumnLeftTriggered()
{
    if (DADataOperateOfDataFrameWidget* dfopt = getCurrentDataFrameOperateWidget()) {
        dfopt->insertColumnLeftBySelect();
    }
}

/**
 * @brief dataframe列重命名
 */
void DAAppRibbonArea::onActionRenameColumnsTriggered()
{
    if (DADataOperateOfDataFrameWidget* dfopt = getCurrentDataFrameOperateWidget()) {
        dfopt->renameColumns();
    }
}

/**
 * @brief 创建数据描述
 */
void DAAppRibbonArea::onActionCreateDataDescribeTriggered()
{
    // TODO 此函数应该移动到dataOperateWidget中
    if (DADataOperateOfDataFrameWidget* dfopt = getCurrentDataFrameOperateWidget()) {
        DAPyDataFrame df = dfopt->createDataDescribe();
        if (df.isNone()) {
            return;
        }
        DAData data = df;
        data.setName(tr("%1_Describe").arg(dfopt->data().getName()));
        data.setDescribe(tr("Generate descriptive statistics that summarize the central tendency, dispersion and "
                            "shape of a [%1]’s distribution, excluding NaN values")
                         .arg(dfopt->data().getName()));
        m_datas->addData(data);
        // showDataOperate要在m_dataManagerStack.push之后，因为m_dataManagerStack.push可能会导致data的名字改变
        m_dockArea->showDataOperateWidget(data);
    }
}

/**
 * @brief dataframe的列数据类型改变
 * @param index
 */
void DAAppRibbonArea::onComboxColumnTypesCurrentDTypeChanged(const DA::DAPyDType& dt)
{
    if (DADataOperateOfDataFrameWidget* dfopt = getCurrentDataFrameOperateWidget()) {
        dfopt->changeSelectColumnType(dt);
    }
}

/**
 * @brief 选中列转换为数值
 */
void DAAppRibbonArea::onActionCastToNumTriggered()
{
    if (DADataOperateOfDataFrameWidget* dfopt = getCurrentDataFrameOperateWidget()) {
        dfopt->castSelectToNum();
    }
}

/**
 * @brief 选中列转换为文字
 */
void DAAppRibbonArea::onActionCastToStringTriggered()
{
    DAAPPRIBBONAREA_PASS();
}

/**
 * @brief 选中列转换为日期
 */
void DAAppRibbonArea::onActionCastToDatetimeTriggered()
{
    if (DADataOperateOfDataFrameWidget* dfopt = getCurrentDataFrameOperateWidget()) {
        dfopt->castSelectToDatetime();
    }
}

/**
 * @brief 选中列转换为索引
 */
void DAAppRibbonArea::onActionChangeToIndexTriggered()
{
    if (DADataOperateOfDataFrameWidget* dfopt = getCurrentDataFrameOperateWidget()) {
        dfopt->changeSelectColumnToIndex();
    }
}

/**
 * @brief 显示工作流区域
 */
void DAAppRibbonArea::onActionShowWorkFlowAreaTriggered()
{
    m_dockArea->raiseDockByWidget((QWidget*)(m_dockArea->getWorkFlowOperateWidget()));
}

/**
 * @brief 显示绘图区域
 */
void DAAppRibbonArea::onActionShowChartAreaTriggered()
{
    m_dockArea->raiseDockByWidget((QWidget*)(m_dockArea->getChartOperateWidget()));
}

/**
 * @brief 显示数据区域
 */
void DAAppRibbonArea::onActionShowDataAreaTriggered()
{
    m_dockArea->raiseDockByWidget((QWidget*)(m_dockArea->getDataOperateWidget()));
}

/**
 * @brief 显示信息区域
 */
void DAAppRibbonArea::onActionShowMessageLogViewTriggered()
{
    m_dockArea->raiseDockByWidget((QWidget*)(m_dockArea->getMessageLogViewWidget()));
}

/**
 * @brief 显示设置区域
 */
void DAAppRibbonArea::onActionSettingWidgetTriggered()
{
    m_dockArea->raiseDockByWidget((QWidget*)(m_dockArea->getSettingContainerWidget()));
}

/**
 * @brief 新建工作流
 */
void DAAppRibbonArea::onActionNewWorkflowTriggered()
{
    bool ok = false;
    QString text = QInputDialog::getText(app(), tr("新工作流名称"), tr("新工作流名称:"), QLineEdit::Normal, QString(), &ok);
    if (!ok || text.isEmpty()) {
        return;
    }
    DAWorkFlowOperateWidget* wf = m_dockArea->getWorkFlowOperateWidget();
    wf->appendWorkflow(text);
}

/**
 * @brief 绘制矩形
 *
 * * @note 绘制完成后会触发onWorkFlowGraphicsSceneMouseActionFinished，在此函数中把这个状态消除
 * @param on
 */
void DAAppRibbonArea::onActionStartDrawRectTriggered(bool on)
{
    if (on) {
        m_dockArea->getWorkFlowOperateWidget()->setMouseActionFlag(DAWorkFlowGraphicsScene::StartAddRect, false);
    }
}

/**
 * @brief 绘制文本
 *
 * @note 绘制完成后会触发onWorkFlowGraphicsSceneMouseActionFinished，在此函数中把这个状态消除
 * @param on
 */
void DAAppRibbonArea::onActionStartDrawTextTriggered(bool on)
{
    if (on) {
        m_dockArea->getWorkFlowOperateWidget()->setMouseActionFlag(DAWorkFlowGraphicsScene::StartAddText, false);
    }
}
