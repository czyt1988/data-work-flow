﻿#ifndef DAPROJECT_H
#define DAPROJECT_H
#include <QObject>
#include "DAProjectInterface.h"
#include "DAGlobals.h"
#include "DAAbstractNodeLinkGraphicsItem.h"

namespace DA
{
DA_IMPL_FORWARD_DECL(DAProject)
class DAWorkFlowOperateWidget;
class DAWorkFlowGraphicsScene;

/**
 * @brief 负责整个节点的工程管理
 */
class DAProject : public DAProjectInterface
{
    Q_OBJECT
    DA_IMPL(DAProject)
public:
    DAProject(DACoreInterface* c, QObject* p = nullptr);
    ~DAProject();
    //设置工作流操作窗口
    void setWorkFlowOperateWidget(DAWorkFlowOperateWidget* w);
    //加载工程
    virtual bool load(const QString& path) override;
    //保存工程
    virtual bool save(const QString& path) override;
    //清除工程
    virtual void clear() override;
    //追加一个工厂的工作流进入本工程中，注意这个操作不会清空当前的工作流
    bool appendProjectWorkflow(const QString& path, bool skipIndex = false);

public:
    //获取工程文件的后缀
    static QString getProjectFileSuffix();
};
}  // namespace DA
#endif  // FCPROJECT_H
