﻿#include "DAChartCanvas.h"
#include <QPainter>
#include <QPaintEvent>
#include <QDebug>
namespace DA
{
class DAChartCanvasPrivate
{
    DA_IMPL_PUBLIC(DAChartCanvas)
public:
    QBrush canvasBackBrush;
    QColor canvasBorderColor;
    DAChartCanvasPrivate(DAChartCanvas* p) : q_ptr(p), canvasBackBrush(Qt::white), canvasBorderColor(Qt::black)
    {
    }

    ~DAChartCanvasPrivate()
    {
    }

    void setqss()
    {
        QString qss = QString("SAPlotCanvas"
                              "{"
                              "   background-color: %1;"
                              "   border: %2px solid %3;"
                              "}")
                      .arg(this->canvasBackBrush.color().name(QColor::HexArgb))
                      .arg(q_ptr->lineWidth())
                      .arg(this->canvasBorderColor.name(QColor::HexArgb));

        q_ptr->setStyleSheet(qss);
    }
};

DAChartCanvas::DAChartCanvas(QwtPlot* p) : QwtPlotCanvas(p), d_ptr(new DAChartCanvasPrivate(this))
{
}

DAChartCanvas::~DAChartCanvas()
{
}

/**
 * @brief 获取背景画刷
 * @return
 */
QBrush DAChartCanvas::getCanvaBackBrush() const
{
    if (testAttribute(Qt::WA_StyledBackground)) {
        return (d_ptr->canvasBackBrush);
    }

    return (palette().brush(QPalette::Normal, QPalette::Window));
}

/**
 * @brief 设置背景画刷
 * @param b 画刷
 */
void DAChartCanvas::setCanvaBackBrush(const QBrush& b)
{
    if (testAttribute(Qt::WA_StyledBackground)) {
        d_ptr->canvasBackBrush = b;
        d_ptr->setqss();
    } else {
        QPalette pal = palette();
        pal.setBrush(QPalette::Window, b);

        setPalette(pal);
    }
}

/**
 * @brief 设置边框颜色
 * @return
 */
QColor DAChartCanvas::getCanvasBorderColor() const
{
    return (d_ptr->canvasBorderColor);
}

/**
 * @brief 获取边框颜色
 * @param c
 */
void DAChartCanvas::setCanvasBorderColor(const QColor& c)
{
    d_ptr->canvasBorderColor = c;
    d_ptr->setqss();
}

/**
 * @brief 设置qss，在Qt::WA_StyledBackground生效时
 */
void DAChartCanvas::useQss()
{
    d_ptr->setqss();
}

void DAChartCanvas::paintEvent(QPaintEvent* e)
{
    //    QPainter p(this);

    //    if (testAttribute(Qt::WA_StyledBackground)) {
    //        setAttribute(Qt::WA_StyledBackground, false);
    //    }
    QwtPlotCanvas::paintEvent(e);
}
}  // End Of Namespace DA
