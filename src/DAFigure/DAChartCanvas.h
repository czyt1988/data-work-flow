﻿#ifndef DACHARTCANVAS_H
#define DACHARTCANVAS_H
#include "DAFigureAPI.h"
#include "qwt_plot_canvas.h"
class QPaintEvent;
namespace DA
{
DA_IMPL_FORWARD_DECL(DAChartCanvas)

/**
 * @brief 重写了paint背景的方法
 */
class DAFIGURE_API DAChartCanvas : public QwtPlotCanvas
{
    Q_OBJECT
    DA_IMPL(DAChartCanvas)
public:
    explicit DAChartCanvas(QwtPlot* p = nullptr);
    virtual ~DAChartCanvas();
    //获取背景
    QBrush getCanvaBackBrush() const;
    void setCanvaBackBrush(const QBrush& b);

    //设置边框颜色
    QColor getCanvasBorderColor() const;
    void setCanvasBorderColor(const QColor& c);

    //设置qss
    void useQss();

protected:
    void paintEvent(QPaintEvent* e) override;
};
}  // End Of Namespace DA
#endif  // DACHARTCANVAS_H
