﻿#include "DAChartWidget.h"
#include <algorithm>
#include <QDebug>
#include <QStyle>
#include <QStyleOption>

#include "qwt_interval.h"
#include "qwt_picker_machine.h"
#include "qwt_legend_label.h"
#include "qwt_date_scale_draw.h"
#include "qwt_plot_histogram.h"
#include "qwt_plot_barchart.h"
#include "qwt_plot_intervalcurve.h"
#include "qwt_plot_multi_barchart.h"
#include "qwt_plot_tradingcurve.h"
#include "qwt_plot_spectrocurve.h"
#include "qwt_plot_spectrogram.h"
#include "DAChartYValueMarker.h"
#include "DAChartPointMarker.h"
#include "DAChartYDataPicker.h"
#include "DAChartXYDataPicker.h"
#include "DAChartCrossTracker.h"
#include "DAChartCanvas.h"

#include "DAChartUtil.h"

// unsigned int ChartWave_qwt::staticValue_nAutoLineID = 0;//静态变量初始化
namespace DA
{
class DAChartWidgetPrivate
{
    DA_IMPL_PUBLIC(DAChartWidget)
public:
    QScopedPointer< QwtPlotZoomer > m_zoomer;
    QScopedPointer< QwtPlotZoomer > m_zoomerSecond;
    QwtPlotGrid* m_grid;
    QwtPlotPicker* m_picker;
    QwtPlotPanner* m_panner;
    QwtPlotLegendItem* m_legend;
    QwtLegend* m_legendPanel;
    DAChartYDataPicker* m_yDataPicker;
    DAChartXYDataPicker* m_xyDataPicker;
    DAChartWidgetPrivate(DAChartWidget* p)
        : q_ptr(p)
        , m_grid(nullptr)
        , m_picker(nullptr)
        , m_panner(nullptr)
        , m_legend(nullptr)
        , m_legendPanel(nullptr)
        , m_yDataPicker(nullptr)
        , m_xyDataPicker(nullptr)
    {
    }
};

DAChartWidget::DAChartWidget(QWidget* parent) : QwtPlot(parent), d_ptr(new DAChartWidgetPrivate(this))
{
    setAutoReplot(false);
    setAutoFillBackground(true);

    QwtPlotLayout* pLayout = plotLayout();

    pLayout->setCanvasMargin(0);
    pLayout->setAlignCanvasToScales(true);

    DAChartCanvas* pCanvas = new DAChartCanvas();

    pCanvas->setFrameStyle(QFrame::Box);
    pCanvas->setLineWidth(1);
    pCanvas->setBorderRadius(0);  //设置圆角为0
    pCanvas->setCursor(Qt::ArrowCursor);
    setCanvas(pCanvas);
    pCanvas->setFocusPolicy(Qt::ClickFocus);
    //设置点击Canvas，plot获得焦点
    pCanvas->setFocusProxy(this);
    setFocusPolicy(Qt::ClickFocus);
    setFrameShape(QFrame::Box);
    setLineWidth(0);

    setAutoReplot(true);
    QwtScaleWidget* ax = axisWidget(QwtPlot::yLeft);

    if (ax) {
        ax->setMargin(0);
    }
    ax = axisWidget(QwtPlot::xBottom);
    if (ax) {
        ax->setMargin(0);
    }
}

DAChartWidget::~DAChartWidget()
{
}

void DAChartWidget::resizeEvent(QResizeEvent* event)
{
    QwtPlot::resizeEvent(event);

    // Qt 4.7.1: QGradient::StretchToDeviceMode is buggy on X11
    // updateGradient();
}

QwtPlotZoomer* DAChartWidget::zoomer()
{
    DA_D(DAChartWidget, d);
    return (d->m_zoomer.data());
}

QwtPlotZoomer* DAChartWidget::zoomerSecond()
{
    DA_D(DAChartWidget, d);
    return (d->m_zoomerSecond.data());
}

QwtPlotGrid* DAChartWidget::grid()
{
    DA_D(DAChartWidget, d);
    return (d->m_grid);
}

bool DAChartWidget::isEnableGrid() const
{
    DA_DC(DAChartWidget, d);
    if (d->m_grid) {
        return (d->m_grid->isVisible());
    }
    return (false);
}

bool DAChartWidget::isEnableGridX() const
{
    DA_DC(DAChartWidget, d);
    if (d->m_grid) {
        if (d->m_grid->isVisible()) {
            return (d->m_grid->xEnabled());
        }
    }
    return (false);
}

bool DAChartWidget::isEnableGridY() const
{
    DA_DC(DAChartWidget, d);
    if (d->m_grid) {
        if (d->m_grid->isVisible()) {
            return (d->m_grid->yEnabled());
        }
    }
    return (false);
}

bool DAChartWidget::isEnableGridXMin() const
{
    DA_DC(DAChartWidget, d);
    if (d->m_grid) {
        if (d->m_grid->isVisible()) {
            return (d->m_grid->xMinEnabled());
        }
    }
    return (false);
}

bool DAChartWidget::isEnableGridYMin() const
{
    DA_DC(DAChartWidget, d);
    if (d->m_grid) {
        if (d->m_grid->isVisible()) {
            return (d->m_grid->yMinEnabled());
        }
    }
    return (false);
}

bool DAChartWidget::isEnablePanner() const
{
    DA_DC(DAChartWidget, d);
    if (d->m_panner) {
        return (d->m_panner->isEnabled());
    }
    return (false);
}

bool DAChartWidget::isEnableLegend() const
{
    DA_DC(DAChartWidget, d);
    if (d->m_legend) {
        return (d->m_legend->isVisible());
    }
    return (false);
}

bool DAChartWidget::isEnableLegendPanel() const
{
    DA_DC(DAChartWidget, d);
    if (d->m_legendPanel) {
        return (d->m_legendPanel->isVisible());
    }
    return (false);
}

bool DAChartWidget::isEnableYDataPicker() const
{
    DA_DC(DAChartWidget, d);
    if (d->m_yDataPicker) {
        return (d->m_yDataPicker->isEnabled());
    }
    return (false);
}

bool DAChartWidget::isEnableXYDataPicker() const
{
    DA_DC(DAChartWidget, d);
    if (d->m_xyDataPicker) {
        return (d->m_xyDataPicker->isEnabled());
    }
    return (false);
}

//========================================================================================
//网格 grid 操作
//========================================================================================
QwtPlotGrid* DAChartWidget::setupGrid(const QColor& color, qreal width, Qt::PenStyle style)
{
    DA_D(DAChartWidget, d);
    bool bShouldAttachAgain(false);

    if (nullptr == d->m_grid) {
        d->m_grid          = new QwtPlotGrid;
        bShouldAttachAgain = true;
    }
    //大刻度显示网格- 所谓大刻度是值刻度上带数值的
    d->m_grid->setMajorPen(color, width, style);
    d->m_grid->setMinorPen(color, 0, Qt::DotLine);  //小刻度的样式
    if (bShouldAttachAgain) {
        d->m_grid->attach(this);
    }
    return (d->m_grid);
}

void DAChartWidget::deleteGrid()
{
    DA_D(DAChartWidget, d);
    if (nullptr == d->m_grid) {
        return;
    }
    d->m_grid->detach();
    delete d->m_grid;
    d->m_grid = nullptr;
    replot();  //刷新，否则不显示
}

void DAChartWidget::enableGrid(bool isShow)
{
    DA_D(DAChartWidget, d);
    if (isShow) {
        if (nullptr == d->m_grid) {
            setupGrid();
        }
        d->m_grid->enableX(true);
        d->m_grid->enableY(true);
        d->m_grid->show();
        emit enableGridXChanged(isShow);
        emit enableGridYChanged(isShow);
        emit enableGridChanged(isShow);
        return;
    } else {
        if (nullptr == d->m_grid) {
            return;
        }
        d->m_grid->hide();
    }
    replot();
    emit enableGridChanged(isShow);
}

void DAChartWidget::enableGridX(bool enable)
{
    DA_D(DAChartWidget, d);
    if (nullptr == d->m_grid) {
        return;
    }
    d->m_grid->enableX(enable);
    emit enableGridXChanged(enable);

    if (!enable) {
        emit enableGridXMinChanged(false);
    }
    // m_grid->show();//刷新
}

void DAChartWidget::enableGridY(bool enable)
{
    DA_D(DAChartWidget, d);
    if (nullptr == d->m_grid) {
        return;
    }
    d->m_grid->enableY(enable);
    emit enableGridYChanged(enable);

    if (!enable) {
        emit enableGridYMinChanged(false);
    }
}

void DAChartWidget::enableGridXMin(bool enable)
{
    DA_D(DAChartWidget, d);
    if (nullptr == d->m_grid) {
        return;
    }
    d->m_grid->enableXMin(enable);
    emit enableGridXMinChanged(enable);
}

void DAChartWidget::enableGridYMin(bool enable)
{
    DA_D(DAChartWidget, d);
    if (nullptr == d->m_grid) {
        return;
    }
    d->m_grid->enableYMin(enable);
    emit enableGridYMinChanged(enable);
}

//========================================================================================
//画线和数据 操作
//========================================================================================

///
/// \brief 设置y值横线标记
/// \param data 值
/// \param strLabel 描述
/// \param clr 颜色
/// \todo type show be use
///
void DAChartWidget::markYValue(double data, const QString& strLabel, QColor clr)
{
    double x                       = axisXmax();
    DAChartYValueMarker* valueMark = new DAChartYValueMarker(data);

    valueMark->setXValue(x);
    valueMark->setLinePen(clr, 1);
    valueMark->setLabel(strLabel);
    valueMark->setLabelAlignment(Qt::AlignTop | Qt::AlignRight);
    valueMark->setSpacing(1);  //设置文字和mark的间隔
    valueMark->attach(this);
}

double DAChartWidget::axisXmin(int axisId) const
{
    QwtInterval inl = axisInterval(axisId);

    if (inl.isValid()) {
        return (inl.minValue());
    }
    axisId = ((axisId == QwtPlot::xBottom) ? QwtPlot::xTop : QwtPlot::xBottom);
    inl    = axisInterval(axisId);
    if (inl.isValid()) {
        return (inl.minValue());
    }
    return (double());
}

double DAChartWidget::axisXmax(int axisId) const
{
    QwtInterval inl = axisInterval(axisId);

    if (inl.isValid()) {
        return (inl.maxValue());
    }
    axisId = ((axisId == QwtPlot::xBottom) ? QwtPlot::xTop : QwtPlot::xBottom);
    inl    = axisInterval(axisId);
    if (inl.isValid()) {
        return (inl.maxValue());
    }
    return (double());
}

double DAChartWidget::axisYmin(int axisId) const
{
    QwtInterval inl = axisInterval(axisId);

    if (inl.isValid()) {
        return (inl.minValue());
    }
    axisId = ((axisId == QwtPlot::yLeft) ? QwtPlot::yRight : QwtPlot::yLeft);
    inl    = axisInterval(axisId);
    if (inl.isValid()) {
        return (inl.minValue());
    }
    return (double());
}

double DAChartWidget::axisYmax(int axisId) const
{
    QwtInterval inl = axisInterval(axisId);

    if (inl.isValid()) {
        return (inl.maxValue());
    }
    axisId = ((axisId == QwtPlot::yLeft) ? QwtPlot::yRight : QwtPlot::yLeft);
    inl    = axisInterval(axisId);
    if (inl.isValid()) {
        return (inl.maxValue());
    }
    return (double());
}

///
/// \brief 此功能用于禁止所有活动的editor，如Zoomer，Picker，Panner，DataPicker等
///
void DAChartWidget::setEnableAllEditor(bool enable)
{
    if (isEnableZoomer()) {
        enableZoomer(enable);
    }
    if (isEnablePicker()) {
        enablePicker(enable);
    }
    if (isEnablePanner()) {
        enablePanner(enable);
    }
    if (isEnableYDataPicker()) {
        enableYDataPicker(enable);
    }
    if (isEnableXYDataPicker()) {
        enableXYDataPicker(enable);
    }
}

void DAChartWidget::setupPicker()
{
    DA_D(DAChartWidget, d);
    if (nullptr == d->m_picker) {
        d->m_picker = new DAChartCrossTracker(this->canvas());
    }
}

void DAChartWidget::enablePicker(bool enable)
{
    DA_D(DAChartWidget, d);
    if (!enable && (nullptr == d->m_picker)) {
        return;
    }
    if (nullptr == d->m_picker) {
        setupPicker();
    }
    d->m_picker->setEnabled(enable);
    if (d->m_zoomer.isNull()) {
        if (isEnableZoomer()) {
            d->m_zoomer->setTrackerMode((enable ? QwtPicker::AlwaysOff : QwtPicker::AlwaysOn));
        }
    }
    emit enablePickerChanged(enable);
}

void DAChartWidget::enablePanner(bool enable)
{
    DA_D(DAChartWidget, d);
    if (enable) {
        if (nullptr == d->m_panner) {
            setupPanner();
        }
        d->m_panner->setEnabled(enable);
    } else {
        if (nullptr != d->m_panner) {
            d->m_panner->setEnabled(enable);
        }
    }
    emit enablePannerChanged(enable);
}

///
/// \brief 建立一个内置的Panner(拖动)，默认使用鼠标中键
///
void DAChartWidget::setupPanner()
{
    //设置拖动
    DA_D(DAChartWidget, d);
    if (nullptr == d->m_panner) {
        d->m_panner = new QwtPlotPanner(canvas());
        d->m_panner->setCursor(QCursor(Qt::ClosedHandCursor));
        d->m_panner->setMouseButton(Qt::LeftButton);
    }
}

void DAChartWidget::deletePanner()
{
    DA_D(DAChartWidget, d);
    if (nullptr != d->m_panner) {
        d->m_panner->setParent(nullptr);  //断开和canvas()的父子连接
        delete d->m_panner;
        d->m_panner = nullptr;
    }
}

///
/// \brief 允许缩放
/// \param enable
///
void DAChartWidget::enableZoomer(bool enable)
{
    DA_D(DAChartWidget, d);
    if (!enable) {
        if (d->m_zoomer.isNull()) {
            return;
        }
    }
    if (d->m_zoomer.isNull() /*|| nullptr == m_zoomerSecond*/) {
        setupZoomer();
    }
    enableZoomer(d->m_zoomer.data(), enable);
    enableZoomer(d->m_zoomerSecond.data(), enable);
    if (isEnablePicker()) {
        d->m_zoomer->setTrackerMode((enable ? QwtPicker::AlwaysOff : QwtPicker::ActiveOnly));
    }
    emit enableZoomerChanged(enable);
}

void DAChartWidget::enableZoomer(QwtPlotZoomer* zoomer, bool enable)
{
    if (nullptr == zoomer) {
        return;
    }
    if (enable) {
        zoomer->setEnabled(true);
        zoomer->setZoomBase(true);
        zoomer->setRubberBand(QwtPicker::RectRubberBand);
        zoomer->setTrackerMode((isEnablePicker() ? QwtPicker::AlwaysOff : QwtPicker::ActiveOnly));
    } else {
        zoomer->setEnabled(false);
        zoomer->setRubberBand(QwtPicker::NoRubberBand);
        zoomer->setTrackerMode(QwtPicker::AlwaysOff);
    }
    if (isEnablePicker()) {
        zoomer->setTrackerMode((enable ? QwtPicker::AlwaysOff : QwtPicker::ActiveOnly));
    }
}

///
/// \brief 回到放大的最底栈
///
void DAChartWidget::setZoomBase()
{
    DA_D(DAChartWidget, d);
    if (!d->m_zoomer.isNull()) {
        d->m_zoomer->setZoomBase(true);
    }
    if (!d->m_zoomerSecond.isNull()) {
        d->m_zoomerSecond->setZoomBase(true);
    }
}

void DAChartWidget::setupZoomer()
{
    DA_D(DAChartWidget, d);
    if (d->m_zoomer.isNull()) {
        qDebug() << "setup zoom";
#if 0
        Zoomer_qwt *zoom = new Zoomer_qwt(xBottom, yLeft, canvas());//Zoomer_qwt( QwtPlot::xBottom, QwtPlot::yLeft,canvas() );

        zoom->on_enable_scrollBar(isHaveScroll);
        zoom->setRubberBand(QwtPicker::RectRubberBand);
        zoom->setTrackerPen(QColor(Qt::black));
        zoom->setKeyPattern(QwtEventPattern::KeyRedo, Qt::Key_I, Qt::ShiftModifier);
        zoom->setKeyPattern(QwtEventPattern::KeyUndo, Qt::Key_O, Qt::ShiftModifier);
        zoom->setKeyPattern(QwtEventPattern::KeyHome, Qt::Key_Home);
        zoom->setMousePattern(QwtEventPattern::MouseSelect2,
            Qt::RightButton, Qt::ControlModifier);
        zoom->setMousePattern(QwtEventPattern::MouseSelect3,
            Qt::RightButton);
        zoom->setTrackerMode(QwtPicker::AlwaysOff);
        m_zoomer = zoom;
#else
        d->m_zoomer.reset(new QwtPlotZoomer(xBottom, yLeft, canvas()));
        d->m_zoomer->setKeyPattern(QwtEventPattern::KeyRedo, Qt::Key_I, Qt::ShiftModifier);
        d->m_zoomer->setKeyPattern(QwtEventPattern::KeyUndo, Qt::Key_O, Qt::ShiftModifier);
        d->m_zoomer->setKeyPattern(QwtEventPattern::KeyHome, Qt::Key_Home);
        d->m_zoomer->setMousePattern(QwtEventPattern::MouseSelect2, Qt::RightButton, Qt::ControlModifier);
        d->m_zoomer->setMousePattern(QwtEventPattern::MouseSelect3, Qt::RightButton);
        d->m_zoomer->setTrackerMode(QwtPicker::AlwaysOff);
        d->m_zoomer->setZoomBase(false);
        d->m_zoomer->setMaxStackDepth(20);
#endif
    }
    if (nullptr == d->m_zoomerSecond) {
        d->m_zoomerSecond.reset(new QwtPlotZoomer(xTop, yRight, canvas()));
        d->m_zoomerSecond->setKeyPattern(QwtEventPattern::KeyRedo, Qt::Key_I, Qt::ShiftModifier);
        d->m_zoomerSecond->setKeyPattern(QwtEventPattern::KeyUndo, Qt::Key_O, Qt::ShiftModifier);
        d->m_zoomerSecond->setKeyPattern(QwtEventPattern::KeyHome, Qt::Key_Home);
        d->m_zoomerSecond->setMousePattern(QwtEventPattern::MouseSelect2, Qt::RightButton, Qt::ControlModifier);
        d->m_zoomerSecond->setMousePattern(QwtEventPattern::MouseSelect3, Qt::RightButton);
        d->m_zoomerSecond->setTrackerMode(QwtPicker::AlwaysOff);
        d->m_zoomerSecond->setZoomBase(false);
        d->m_zoomerSecond->setMaxStackDepth(20);
    }
    QwtPlotMagnifier* magnifier = new QwtPlotMagnifier(canvas());
    magnifier->setMouseButton(Qt::NoButton);
}

void DAChartWidget::deleteZoomer()
{
    DA_D(DAChartWidget, d);
    if (!d->m_zoomer.isNull()) {
        d->m_zoomer.reset();
    }
    if (!d->m_zoomerSecond.isNull()) {
        d->m_zoomerSecond.reset();
    }
}

///
/// \brief 设置缩放重置
///
void DAChartWidget::setZoomReset()
{
    DA_D(DAChartWidget, d);
    if (!d->m_zoomer.isNull()) {
        d->m_zoomer->zoom(0);
    }
    if (!d->m_zoomerSecond.isNull()) {
        d->m_zoomerSecond->zoom(0);
    }
}

void DAChartWidget::zoomIn()
{
    DA_D(DAChartWidget, d);
    if (d->m_zoomer.isNull()) {
        setupZoomer();
    }

    QRectF rect = d->m_zoomer->zoomRect();
    double w    = rect.width() * 0.625;
    double h    = rect.height() * 0.625;
    double x    = rect.x() + (rect.width() - w) / 2.0;
    double y    = rect.y() + (rect.height() - h) / 2.0;

    rect.setX(x);
    rect.setY(y);
    rect.setWidth(w);
    rect.setHeight(h);

    d->m_zoomer->zoom(rect);
}

void DAChartWidget::zoomOut()
{
    DA_D(DAChartWidget, d);
    if (d->m_zoomer.isNull()) {
        setupZoomer();
    }

    QRectF rect = d->m_zoomer->zoomRect();
    double w    = rect.width() * 1.6;
    double h    = rect.height() * 1.6;
    double x    = rect.x() - (w - rect.width()) / 2.0;
    double y    = rect.y() - (h - rect.height()) / 2.0;

    rect.setX(x);
    rect.setY(y);
    rect.setWidth(w);
    rect.setHeight(h);
    d->m_zoomer->zoom(rect);
}

///
/// \brief 缩放到最适合比例，就是可以把所有图都能看清的比例
///
void DAChartWidget::zoomInCompatible()
{
    QwtInterval intv[ axisCnt ];

    DAChartUtil::dataRange(this, &intv[ yLeft ], &intv[ yRight ], &intv[ xBottom ], &intv[ xTop ]);
    if (!d_ptr->m_zoomer.isNull()) {
        int axx = d_ptr->m_zoomer->xAxis();
        int axy = d_ptr->m_zoomer->yAxis();
        QRectF rect1;
        rect1.setRect(intv[ axx ].minValue(), intv[ axy ].minValue(), intv[ axx ].width(), intv[ axy ].width());
        qDebug() << rect1;
        d_ptr->m_zoomer->zoom(rect1);
    }
    if (!d_ptr->m_zoomerSecond.isNull()) {
        int axx = d_ptr->m_zoomerSecond->xAxis();
        int axy = d_ptr->m_zoomerSecond->yAxis();
        QRectF rect1;
        rect1.setRect(intv[ axx ].minValue(), intv[ axy ].minValue(), intv[ axx ].width(), intv[ axy ].width());
        qDebug() << rect1;
        d_ptr->m_zoomerSecond->zoom(rect1);
    }

    /* !此方法不行
     * if(!d->m_zoomer.isNull())
     * {
     *  int axx = d->m_zoomer->xAxis();
     *  int axy = d->m_zoomer->yAxis();
     *  double xmin = axisScaleEngine(axx)->lowerMargin();
     *  double xmax = axisScaleEngine(axx)->upperMargin();
     *  double ymin = axisScaleEngine(axy)->lowerMargin();
     *  double ymax = axisScaleEngine(axy)->upperMargin();
     *  QRectF rect1;
     *  rect1.setRect(xmin,ymin,xmax-xmin,ymax-ymin);
     *  qDebug()<<rect1;
     *  d->m_zoomer->zoom(rect1);
     * }
     */
}

void DAChartWidget::setupLegend()
{
    if (nullptr == d_ptr->m_legend) {
        d_ptr->m_legend = new QwtPlotLegendItem();
        d_ptr->m_legend->setRenderHint(QwtPlotItem::RenderAntialiased);
        QColor color(Qt::white);
        d_ptr->m_legend->setTextPen(color);
        d_ptr->m_legend->setBorderPen(color);
        QColor c(Qt::gray);
        c.setAlpha(200);
        d_ptr->m_legend->setBackgroundBrush(c);
        d_ptr->m_legend->attach(this);
    }
}

void DAChartWidget::enableLegend(bool enable)
{
    if (enable) {
        if (d_ptr->m_legend) {
            d_ptr->m_legend->setVisible(true);
        } else {
            setupLegend();
        }
    } else {
        if (d_ptr->m_legend) {
            d_ptr->m_legend->setVisible(false);
            // 			m_legend->detach();
            // 			delete m_legend;
            // 			m_legend = nullptr;
        }
    }
    emit enableLegendChanged(enable);
}

void DAChartWidget::enableLegendPanel(bool enable)
{
    if (enable) {
        setuplegendPanel();
    } else {
        deletelegendPanel();
    }
    emit enableLegendPanelChanged(enable);
}

void DAChartWidget::setuplegendPanel()
{
    if (d_ptr->m_legendPanel) {
        return;
    }
    d_ptr->m_legendPanel = new QwtLegend;
    d_ptr->m_legendPanel->setDefaultItemMode(QwtLegendData::Checkable);
    insertLegend(d_ptr->m_legendPanel, QwtPlot::RightLegend);
    //	connect( m_legendPanel, &QwtLegend::checked,&ChartWave_qwt::showItem);
    connect(d_ptr->m_legendPanel, SIGNAL(checked(const QVariant&, bool, int)), SLOT(showItem(const QVariant&, bool)));

    QwtPlotItemList items = itemList(QwtPlotItem::Rtti_PlotCurve);

    for (int i = 0; i < items.size(); i++) {
        const QVariant itemInfo     = itemToInfo(items[ i ]);
        QwtLegendLabel* legendLabel = qobject_cast< QwtLegendLabel* >(d_ptr->m_legendPanel->legendWidget(itemInfo));
        if (legendLabel) {
            legendLabel->setChecked(items[ i ]->isVisible());
        }
    }
}

void DAChartWidget::deletelegendPanel()
{
    DA_D(DAChartWidget, d);
    insertLegend(nullptr);  //内部会销毁
    d->m_legendPanel = nullptr;
}

void DAChartWidget::setupYDataPicker()
{
    if (nullptr == d_ptr->m_yDataPicker) {
        d_ptr->m_yDataPicker = new DAChartYDataPicker(this->canvas());
        d_ptr->m_yDataPicker->setRubberBandPen(QPen("MediumOrchid"));
    }
}

void DAChartWidget::deleteYDataPicker()
{
    if (nullptr != d_ptr->m_yDataPicker) {
        d_ptr->m_yDataPicker->setParent(nullptr);  //断开和canvas()的父子连接
        delete d_ptr->m_yDataPicker;
        d_ptr->m_yDataPicker = nullptr;
    }
}

void DAChartWidget::setupXYDataPicker()
{
    if (nullptr == d_ptr->m_xyDataPicker) {
        d_ptr->m_xyDataPicker = new DAChartXYDataPicker(this->canvas());
        d_ptr->m_xyDataPicker->setRubberBandPen(QPen("MediumOrchid"));
    }
}

void DAChartWidget::deleteXYDataPicker()
{
    DA_D(DAChartWidget, d);
    if (nullptr != d->m_xyDataPicker) {
        d->m_xyDataPicker->setParent(nullptr);  //断开和canvas()的父子连接
        delete d->m_xyDataPicker;
        d->m_xyDataPicker = nullptr;
    }
}

void DAChartWidget::enableYDataPicker(bool enable)
{
    if (enable) {
        setupYDataPicker();
    } else {
        deleteYDataPicker();
    }
    emit enableYDataPickerChanged(enable);
}

void DAChartWidget::enableXYDataPicker(bool enable)
{
    if (enable) {
        setupXYDataPicker();
    } else {
        deleteXYDataPicker();
    }
    emit enableXYDataPickerChanged(enable);
}

///
/// \brief 是否允许缩放
/// \return
///
bool DAChartWidget::isEnableZoomer() const
{
    DA_DC(DAChartWidget, d);
    if (d->m_zoomer) {
        return (d->m_zoomer->isEnabled());
    }
    if (d->m_zoomerSecond) {
        return (d->m_zoomerSecond->isEnabled());
    }
    return (false);
}

///
/// \brief 是否允许十字光标
/// \return
///
bool DAChartWidget::isEnablePicker() const
{
    DA_DC(DAChartWidget, d);
    if (d->m_picker) {
        return (d->m_picker->isEnabled());
    }
    return (false);
}

void DAChartWidget::showItem(const QVariant& itemInfo, bool on)
{
    QwtPlotItem* plotItem = infoToItem(itemInfo);

    if (plotItem) {
        plotItem->setVisible(on);
    }
}

QList< QwtPlotCurve* > DAChartWidget::getCurveList()
{
    QList< QwtPlotCurve* > curves;
    QwtPlotItemList items = itemList(QwtPlotItem::Rtti_PlotCurve);

    for (int i(0); i < items.size(); ++i) {
        curves.append(static_cast< QwtPlotCurve* >(items[ i ]));
    }
    return (curves);
}

///
/// \brief getMakerList 获取所有标记
/// \return
///
QList< QwtPlotMarker* > DAChartWidget::getMakerList()
{
    QList< QwtPlotMarker* > list;
    QwtPlotItemList items = itemList(QwtPlotItem::Rtti_PlotMarker);

    for (int i(0); i < items.size(); ++i) {
        list.append(static_cast< QwtPlotMarker* >(items[ i ]));
    }
    return (list);
}

QwtDateScaleDraw* DAChartWidget::setAxisDateTimeScale(const QString& format, int axisID, QwtDate::IntervalType intType)
{
    QwtDateScaleDraw* dateScale = setAxisDateTimeScale(axisID);

    dateScale->setDateFormat(intType, format);
    return (dateScale);
}

QwtDateScaleDraw* DAChartWidget::setAxisDateTimeScale(int axisID)
{
    QwtDateScaleDraw* dateScale;

    dateScale = new QwtDateScaleDraw;  //原来的scaleDraw会再qwt自动delete
    setAxisScaleDraw(axisID, dateScale);
    return (dateScale);
}
}  // End Of Namespace DA
