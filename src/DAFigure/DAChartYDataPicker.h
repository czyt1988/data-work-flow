﻿#ifndef DACHARTYDATAPICKER_H
#define DACHARTYDATAPICKER_H
#include "DAFigureAPI.h"
#include <QwtPlotPicker>
#include <QwtText>
#include <QLineF>
class QwtPlotItem;
class QwtPlotCurve;
class QwtPlotBarChart;
namespace DA
{
/**
 * @brief The DAYDataTracker class
 */
class DAFIGURE_API DAChartYDataPicker : public QwtPlotPicker
{
public:
    DAChartYDataPicker(QWidget*);

protected:
    virtual QwtText trackerTextF(const QPointF&) const;
    virtual QRect trackerRect(const QFont&) const;
    //内部实现使用了dynamic_cast
    virtual QString itemInfoAt(const QwtPlotItem* item, const QPointF& pos) const;

private:
    QString curveInfoAt(const QwtPlotCurve*, const QPointF&) const;
    QLineF curveLineAt(const QwtPlotCurve*, double x) const;
    QString barInfoAt(const QwtPlotBarChart*, const QPointF&) const;
    double barValueAt(const QwtPlotBarChart* bar, double x) const;
};
}  // End Of Namespace DA
#endif  // SAYDATATRACKER_H
