#include "DAChartManageWidget.h"
#include "ui_DAChartManageWidget.h"
//===================================================
// using DA namespace -- 禁止在头文件using!!
//===================================================

using namespace DA;

//===================================================
// DAChartManageWidget
//===================================================
DAChartManageWidget::DAChartManageWidget(QWidget* parent) : QWidget(parent), ui(new Ui::DAChartManageWidget)
{
    ui->setupUi(this);
}

DAChartManageWidget::~DAChartManageWidget()
{
    delete ui;
}
