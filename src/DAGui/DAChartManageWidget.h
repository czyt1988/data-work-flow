﻿#ifndef DACHARTMANAGEWIDGET_H
#define DACHARTMANAGEWIDGET_H
#include "DAGuiAPI.h"
#include <QWidget>
namespace Ui
{
class DAChartManageWidget;
}
namespace DA
{
class DAGUI_API DAChartManageWidget : public QWidget
{
    Q_OBJECT

public:
    DAChartManageWidget(QWidget* parent = nullptr);
    ~DAChartManageWidget();

private:
    Ui::DAChartManageWidget* ui;
};
}  // end of namespace DA
#endif  // DACHARTMANAGEWIDGET_H
