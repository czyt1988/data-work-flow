﻿#include "DAChartOperateWidget.h"
#include "ui_DAChartOperateWidget.h"
//===================================================
// using DA namespace -- 禁止在头文件using!!
//===================================================

using namespace DA;

//===================================================
// DAChartOperateWidget
//===================================================
DAChartOperateWidget::DAChartOperateWidget(QWidget* parent) : QWidget(parent), ui(new Ui::DAChartOperateWidget)
{

    ui->setupUi(this);
}

DAChartOperateWidget::~DAChartOperateWidget()
{
    delete ui;
}
