﻿#ifndef DACHARTOPERATEWIDGET_H
#define DACHARTOPERATEWIDGET_H
#include <QWidget>
#include "DAGuiAPI.h"
namespace Ui
{
class DAChartOperateWidget;
}
namespace DA
{
class DAGUI_API DAChartOperateWidget : public QWidget
{
    Q_OBJECT

public:
    DAChartOperateWidget(QWidget* parent = nullptr);
    ~DAChartOperateWidget();

private:
    Ui::DAChartOperateWidget* ui;
};
}  // end of namespace DA
#endif  // DACHARTOPERATEWIDGET_H
