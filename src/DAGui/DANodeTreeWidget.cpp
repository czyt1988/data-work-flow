﻿#include "DANodeTreeWidget.h"
#include <QMouseEvent>
#include <QApplication>
#include <QDrag>
#include "DANodeMimeData.h"
#define ROLE_META_DATA (Qt::UserRole + 1)
namespace DA
{

DANodeTreeWidgetItem::DANodeTreeWidgetItem() : QTreeWidgetItem(ThisItemType)
{
}

DANodeTreeWidgetItem::DANodeTreeWidgetItem(QTreeWidgetItem* parent) : QTreeWidgetItem(parent, ThisItemType)
{
}

DANodeTreeWidgetItem::DANodeTreeWidgetItem(QTreeWidgetItem* parent, const DANodeMetaData& md)
    : QTreeWidgetItem(parent, ThisItemType)
{
    setNodeMetaData(md);
}

DANodeMetaData DANodeTreeWidgetItem::getNodeMetaData() const
{
    return (data(0, ROLE_META_DATA).value< DANodeMetaData >());
}

void DANodeTreeWidgetItem::setNodeMetaData(const DANodeMetaData& md)
{
    setIcon(0, md.getIcon());
    setText(0, md.getNodeName());
    QString tt = QString("<b>%1</b><br/>%2").arg(md.getNodeName(), md.getNodeTooltip());
    setToolTip(0, tt);
    setData(0, ROLE_META_DATA, QVariant::fromValue(md));
}

//===================================================
// DANodeTreeWidget
//===================================================

DANodeTreeWidget::DANodeTreeWidget(QWidget* par) : QTreeWidget(par), _favoriteItem(nullptr)
{
    qRegisterMetaTypeStreamOperators< DANodeMetaData >("DANodeMetaData");
    setDragEnabled(true);  //启用拖放
    setHeaderHidden(true);

}

void DANodeTreeWidget::addItems(const QString& groupName, const QList< DANodeMetaData >& nodeMetaDatas)
{
    QTreeWidgetItem* root = new QTreeWidgetItem({ groupName });
    for (const DANodeMetaData& d : nodeMetaDatas) {
        DANodeTreeWidgetItem* i = new DANodeTreeWidgetItem(root, d);
        Q_UNUSED(i);
    }
    insertTopLevelItem(0, root);
}

/**
 * @brief 添加到收藏
 * @param md
 */
void DANodeTreeWidget::addToFavorite(const DANodeMetaData& md)
{
    QTreeWidgetItem* favItem = createFavoriteItem();
    DANodeTreeWidgetItem* i  = new DANodeTreeWidgetItem(favItem, md);
    Q_UNUSED(i);
}

/**
 * @brief 移除收藏
 * @param md
 */
void DANodeTreeWidget::removeFavorite(const DANodeMetaData& md)
{
    QTreeWidgetItem* favItem = getFavoriteItem();
    int c                    = favItem->childCount();
    QList< QTreeWidgetItem* > needDelete;
    for (int i = 0; i < c; ++i) {
        QTreeWidgetItem* item = favItem->child(i);
        if (DANodeTreeWidgetItem::ThisItemType == item->type()) {
            DANodeTreeWidgetItem* nitem = static_cast< DANodeTreeWidgetItem* >(item);
            if (md == nitem->getNodeMetaData()) {
                needDelete.append(item);
            }
        }
    }
    for (QTreeWidgetItem* i : needDelete) {
        delete i;
    }
}



/**
 * @brief 收藏item
 *
 * @note 如果没有会创建
 * @return
 */
QTreeWidgetItem* DANodeTreeWidget::getFavoriteItem()
{
    if (nullptr == _favoriteItem) {
        return createFavoriteItem();
    }
    return _favoriteItem;
}

QTreeWidgetItem* DANodeTreeWidget::createFavoriteItem()
{
    if (_favoriteItem) {
        return _favoriteItem;
    }
    _favoriteItem = new QTreeWidgetItem({ tr("Favorite") });
    _favoriteItem->setIcon(0, QIcon(":/icon/icon/favorite.svg"));
    insertTopLevelItem(0, _favoriteItem);
    return _favoriteItem;
}

/**
 * @brief 通过位置获取对应的md
 * @param p
 * @return
 */
DANodeMetaData DANodeTreeWidget::getNodeMetaData(const QPoint& p) const
{
    QTreeWidgetItem* item = itemAt(p);
    if (!item) {
        return DANodeMetaData();
    }
    if (DANodeTreeWidgetItem::ThisItemType != item->type()) {
        return DANodeMetaData();
    }
    DANodeTreeWidgetItem* nitem = static_cast< DANodeTreeWidgetItem* >(item);
    return nitem->getNodeMetaData();
}

void DANodeTreeWidget::mousePressEvent(QMouseEvent* event)
{
    if (event->buttons() & Qt::LeftButton) {
        _startPressPos = event->pos();
    }
    QTreeWidget::mousePressEvent(event);
}

void DANodeTreeWidget::mouseMoveEvent(QMouseEvent* event)
{
    if (event->buttons() & Qt::LeftButton) {
        if ((event->pos() - _startPressPos).manhattanLength() > QApplication::startDragDistance()) {
            QTreeWidgetItem* pitem = itemAt(_startPressPos);
            if (DANodeTreeWidgetItem::ThisItemType != pitem->type()) {
                QTreeWidget::mouseMoveEvent(event);
                return;
            }
            DANodeTreeWidgetItem* nitem = static_cast< DANodeTreeWidgetItem* >(pitem);
            DANodeMetaData nodemd       = nitem->getNodeMetaData();
            DANodeMimeData* md          = new DANodeMimeData(nodemd);
            QDrag* drag                 = new QDrag(this);
            drag->setMimeData(md);
            drag->setPixmap(nodemd.getIcon().pixmap(30, 30));
            drag->setHotSpot(QPoint(15, 15));
            drag->exec();
            return;
        }
    }
}

}
