﻿#include "DAProjectInterface.h"
#include <QFileInfo>
namespace DA
{

//===================================================
// DAProjectInterfacePrivate
//===================================================
class DAProjectInterfacePrivate
{
    DA_IMPL_PUBLIC(DAProjectInterface)
public:
    DAProjectInterfacePrivate(DAProjectInterface* p);
    //存在路径
    bool isHaveProjectFilePath() const;

public:
    QFileInfo _projectFileInfo;  ///< 记录工程文件信息
    bool _isDirty;               ///< 脏标识
};

DAProjectInterfacePrivate::DAProjectInterfacePrivate(DAProjectInterface* p) : q_ptr(p)
{
}

bool DAProjectInterfacePrivate::isHaveProjectFilePath() const
{
    return _projectFileInfo.isFile();
}
//===================================================
// DAProjectInterface
//===================================================
DAProjectInterface::DAProjectInterface(DACoreInterface* c, QObject* par)
    : DABaseInterface(c, par), d_ptr(new DAProjectInterfacePrivate(this))
{
}

DAProjectInterface::~DAProjectInterface()
{
}
/**
 * @brief 获取工程名
 *
 * 返回工程的文件名(不含后缀)
 * @return 如果没有设置工程，将返回空字符串
 */
QString DAProjectInterface::getProjectBaseName() const
{
    if (!d_ptr->isHaveProjectFilePath()) {
        return QString();
    }
    return (d_ptr->_projectFileInfo.baseName());
}

/**
 * @brief 获取工程路径
 *
 * @sa setProjectPath
 * @return 如果没有设置工程，将返回空字符串
 */
QString DAProjectInterface::getProjectDir() const
{
    if (!d_ptr->isHaveProjectFilePath()) {
        return QString();
    }
    return d_ptr->_projectFileInfo.absolutePath();
}

/**
 * @brief DAProjectInterface::getProjectFilePath
 * @note 注意这个工程路径是工程文件的路径，并不是工作区的路径，但设置工程路径会把工作区设置到当前目录下
 * @return
 */
QString DAProjectInterface::getProjectFilePath() const
{
    if (!d_ptr->isHaveProjectFilePath()) {
        return QString();
    }
    return d_ptr->_projectFileInfo.absoluteFilePath();
}

/**
 * @brief 设置工程路径
 * @param projectPath
 * @note 注意这个工程路径是工程文件的路径，并不是工作区的路径，但设置工程路径会把工作区设置到当前目录下
 */
void DAProjectInterface::setProjectPath(const QString& projectPath)
{
    d_ptr->_projectFileInfo.setFile(projectPath);
}

/**
 * @brief 获取工作区
 * @note 工程文件所在目录定义为工作区
 * @return 如果没有设置工程，将返回空字符串
 */
QString DAProjectInterface::getWorkingDirectory() const
{
    if (!d_ptr->isHaveProjectFilePath()) {
        return QString();
    }
    return d_ptr->_projectFileInfo.absolutePath();
}
/**
 * @brief 工程是否脏
 * @return
 */
bool DAProjectInterface::isDirty() const
{
    return d_ptr->_isDirty;
}

/**
 * @brief 清空工程
 */
void DAProjectInterface::clear()
{
    setDirty(false);
    d_ptr->_projectFileInfo = QFileInfo();
    emit projectIsCleaned();
}

/**
 * @brief 设置为dirty
 * @param on
 */
void DAProjectInterface::setDirty(bool on)
{
    d_ptr->_isDirty = on;
    emit becomeDirty(on);
}

}
