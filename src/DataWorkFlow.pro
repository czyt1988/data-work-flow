include($$PWD/common.pri)
include($$PWD/function.pri)

system(mkdir $$saFixPath($${BIN_PLUGIN_DIR}))#创建插件路径
# 运行此文件之前，先构建第三方库3rdparty.pro
TEMPLATE = subdirs
SUBDIRS += \
          $$PWD/DAUtils \
          $$PWD/DAMessageHandler \
          $$PWD/DAPyBindQt \
          $$PWD/DAPyScripts \
          $$PWD/DAData \
          $$PWD/DACommonWidgets \
          $$PWD/DAGraphicsView \
          $$PWD/DAWorkFlow \
          $$PWD/DAPyCommonWidgets \
          $$PWD/DAGui \
          $$PWD/DAInterface \
          $$PWD/DAPluginSupport \
          $$PWD/APP
#          $$PWD/DAPlugins/DAUtilityNodePlugin \


CONFIG   += ordered
CODECFORTR = utf-8

TRANSLATIONS += da_zh_CN.ts \
                da_en_US.ts
