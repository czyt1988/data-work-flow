<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>DA::DADataManager</name>
    <message>
        <location filename="DAData/DADataManager.cpp" line="49"/>
        <source>data:%1 have been added</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DANodeLinkItemSettingWidget</name>
    <message>
        <location filename="DAWorkFlow/DANodeLinkItemSettingWidget.cpp" line="21"/>
        <source>Knuckle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeLinkItemSettingWidget.cpp" line="22"/>
        <source>Straight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeLinkItemSettingWidget.cpp" line="23"/>
        <source>Bezier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeLinkItemSettingWidget.cpp" line="167"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DAPluginManager</name>
    <message>
        <location filename="DAPluginSupport/DAPluginManager.cpp" line="80"/>
        <source>plugin dir is:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginSupport/DAPluginManager.cpp" line="82"/>
        <source>start load plugin:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginSupport/DAPluginManager.cpp" line="84"/>
        <source> ignore invalid suffix:%1,file is:%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginSupport/DAPluginManager.cpp" line="89"/>
        <source>can not load plugin:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginSupport/DAPluginManager.cpp" line="147"/>
        <source>Plugin Manager Info:is loaded=%1,plugin counts=%2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DAPyDTypeComboBox</name>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="15"/>
        <source>float64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="16"/>
        <source>float32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="17"/>
        <source>float16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="19"/>
        <source>int64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="20"/>
        <source>uint64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="21"/>
        <source>int32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="22"/>
        <source>uint32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="23"/>
        <source>int16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="24"/>
        <source>uint16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="25"/>
        <source>int8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="26"/>
        <source>uint8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="28"/>
        <source>str</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="30"/>
        <source>bool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="32"/>
        <source>complex64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="33"/>
        <source>complex128</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="35"/>
        <source>datetime64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="36"/>
        <source>timedelta64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="38"/>
        <source>bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyCommonWidgets/DAPyDTypeComboBox.cpp" line="39"/>
        <source>object</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DAWorkFlow</name>
    <message>
        <location filename="DAWorkFlow/DAWorkFlow.cpp" line="349"/>
        <source>begin exec workflow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DAWorkFlow.cpp" line="351"/>
        <source>empty workflow can not exec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DAWorkFlow.cpp" line="356"/>
        <source>execute error:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DAWorkFlow.cpp" line="364"/>
        <source>workflow start run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DAWorkFlow.cpp" line="491"/>
        <source>An exception occurred at the end of workflow preparation:%1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DA::DAWorkFlowExecuter</name>
    <message>
        <location filename="DAWorkFlow/DAWorkFlowExecuter.cpp" line="259"/>
        <location filename="DAWorkFlow/DAWorkFlowExecuter.cpp" line="283"/>
        <source>execute node, name=</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DANodeItemSettingWidget</name>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="14"/>
        <source>Node Item Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="20"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="26"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="46"/>
        <source>Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="66"/>
        <source>Lock Aspect Ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="83"/>
        <source>rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="103"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="109"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="132"/>
        <source>y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="158"/>
        <source>Link Point Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="164"/>
        <source>Input Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="267"/>
        <source>Output Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="373"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="379"/>
        <source>movable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="386"/>
        <source>resizable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeItemSettingWidget.ui" line="396"/>
        <source>tooltip</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DANodeLinkItemSettingWidget</name>
    <message>
        <location filename="DAWorkFlow/DANodeLinkItemSettingWidget.ui" line="14"/>
        <source>Node Link Item Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeLinkItemSettingWidget.ui" line="35"/>
        <source>front style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeLinkItemSettingWidget.ui" line="58"/>
        <source>pen:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeLinkItemSettingWidget.ui" line="81"/>
        <source>link style:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeLinkItemSettingWidget.ui" line="88"/>
        <source>end style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeLinkItemSettingWidget.ui" line="95"/>
        <source>end point size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DANodeSettingWidget</name>
    <message>
        <location filename="DAWorkFlow/DANodeSettingWidget.ui" line="14"/>
        <source>Node Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeSettingWidget.ui" line="20"/>
        <source>Meta Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeSettingWidget.ui" line="26"/>
        <source>Prototype</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeSettingWidget.ui" line="40"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DANodeSettingWidget.ui" line="59"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DAPenEditWidget</name>
    <message>
        <location filename="DACommonWidgets/DAPenEditWidget.cpp" line="105"/>
        <source>Pen Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DACommonWidgets/DAPenEditWidget.cpp" line="106"/>
        <source> px</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExternalDataDialog</name>
    <message>
        <location filename="ExternalDataDialog/ExternalDataDialog.ui" line="14"/>
        <source>ExternalDataDialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExternalDataDialog/ExternalDataDialog.cpp" line="10"/>
        <source>外部数据</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExternalDataDialog/ExternalDataDialog.cpp" line="32"/>
        <source>A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExternalDataDialog/ExternalDataDialog.cpp" line="32"/>
        <source>B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExternalDataDialog/ExternalDataDialog.cpp" line="32"/>
        <source>C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExternalDataDialog/ExternalDataDialog.cpp" line="32"/>
        <source>D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExternalDataDialog/ExternalDataDialog.cpp" line="32"/>
        <source>E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExternalDataDialog/ExternalDataDialog.cpp" line="41"/>
        <source>数据源</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExternalDataDialog/ExternalDataDialog.cpp" line="42"/>
        <source>位置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExternalDataDialog/ExternalDataDialog.cpp" line="43"/>
        <source>标识符</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExternalDataDialog/ExternalDataDialog.cpp" line="44"/>
        <source>主</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExternalDataDialog/ExternalDataDialog.cpp" line="45"/>
        <source>描述</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="DAPyBindQt/numpy/DAPyDType.cpp" line="245"/>
        <location filename="DAPyBindQt/pandas/DAPyIndex.cpp" line="125"/>
        <source>DAPyIndex get python object type is not pandas.Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyBindQt/pandas/DAPyModulePandas.cpp" line="167"/>
        <source>use utf-8 open file %1 error,try to use ansi encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyBindQt/pandas/DAPySeries.cpp" line="145"/>
        <source>DAPySeries  get python object type is not pandas.Series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyScripts/DAPyScripts.cpp" line="62"/>
        <source>Initialized import sys module error:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPyScripts/DAPyScripts.cpp" line="82"/>
        <source>Initialized import py module error:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAData/DAAbstractData.cpp" line="96"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAData/DAAbstractData.cpp" line="98"/>
        <source>object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAData/DAAbstractData.cpp" line="100"/>
        <source>dataframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAData/DAAbstractData.cpp" line="102"/>
        <source>raw</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DACommonWidgets/DAPenStyleComboBox.cpp" line="35"/>
        <source>No Pen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DACommonWidgets/DAPenStyleComboBox.cpp" line="37"/>
        <source>Solid Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DACommonWidgets/DAPenStyleComboBox.cpp" line="39"/>
        <source>Dash Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DACommonWidgets/DAPenStyleComboBox.cpp" line="41"/>
        <source>Dot Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DACommonWidgets/DAPenStyleComboBox.cpp" line="43"/>
        <source>Dash Dot Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DACommonWidgets/DAPenStyleComboBox.cpp" line="45"/>
        <source>Dash Dot Dot Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DACommonWidgets/DAPenStyleComboBox.cpp" line="47"/>
        <source>Custom Dash Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAGraphicsView/DACommandsForGraphics.cpp" line="12"/>
        <source>Item Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAGraphicsView/DACommandsForGraphics.cpp" line="41"/>
        <source>Item Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAGraphicsView/DACommandsForGraphics.cpp" line="82"/>
        <source>Items Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAGraphicsView/DACommandsForGraphics.cpp" line="138"/>
        <source>Item Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAGraphicsView/DACommandsForGraphics.cpp" line="197"/>
        <location filename="DAGraphicsView/DACommandsForGraphics.cpp" line="207"/>
        <source>Item Resize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAGraphicsView/DACommandsForGraphics.cpp" line="278"/>
        <source>Item Resize Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAGraphicsView/DACommandsForGraphics.cpp" line="323"/>
        <source>Item Resize Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAGraphicsView/DACommandsForGraphics.cpp" line="368"/>
        <source>Item Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DAAbstractNodeLinkGraphicsItem.cpp" line="703"/>
        <source>Item have not out put link point:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DAAbstractNodeLinkGraphicsItem.cpp" line="709"/>
        <source>Link from must attach an output point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DAAbstractNodeLinkGraphicsItem.cpp" line="747"/>
        <source>Item have not in put link point:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DAAbstractNodeLinkGraphicsItem.cpp" line="753"/>
        <source>Link to must attach an input point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DACommandsForWorkFlowNodeGraphics.cpp" line="18"/>
        <source>Create Node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DACommandsForWorkFlowNodeGraphics.cpp" line="24"/>
        <source>can not create node,metadata name=%1(%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DACommandsForWorkFlowNodeGraphics.cpp" line="78"/>
        <source>Remove Select Nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DACommandsForWorkFlowNodeGraphics.cpp" line="218"/>
        <location filename="DAWorkFlow/DACommandsForWorkFlowNodeGraphics.cpp" line="231"/>
        <source>Create Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAWorkFlow/DACommandsForWorkFlowNodeGraphics.cpp" line="284"/>
        <source>Remove Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginSupport/DAPluginOption.cpp" line="97"/>
        <source>Failed to load %1 (Reason: %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginSupport/DAPluginOption.cpp" line="106"/>
        <source>cannot resolve plugin_create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginSupport/DAPluginOption.cpp" line="110"/>
        <source>cannot resolve plugin_destory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginSupport/DAPluginOption.cpp" line="204"/>
        <source>plugin file name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginSupport/DAPluginOption.cpp" line="204"/>
        <source>,iid:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginSupport/DAPluginOption.cpp" line="205"/>
        <source>,name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginSupport/DAPluginOption.cpp" line="205"/>
        <source>,description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginSupport/DAPluginOption.cpp" line="206"/>
        <source>,version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAPluginSupport/DAPluginOption.cpp" line="207"/>
        <source>,error string:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DAGraphicsView/DAGraphicsItemFactory.cpp" line="55"/>
        <source>Class name %1 not registered to item factory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
